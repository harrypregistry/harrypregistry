﻿namespace VaccineSurvey.Data.Queries
{
    public class QuestionQuery : IQuestionQuery
    {
        public string Delete() => "DELETE FROM dbo.Questions WHERE Id = @Id;";

        public string Details() => "SELECT TOP(1) q.* FROM dbo.Questions q WHERE Id = @Id;" +
                "SELECT * FROM dbo.QuestionOptions WHERE QuestionId = @Id;";

        public string FindById() => "SELECT TOP(1) q.* FROM dbo.Questions q WHERE Id = @Id;";

        public string GetAll() => "SELECT * FROM dbo.Questions ORDER BY Sequence;";
        public string GetAllChild() => "SELECT * FROM dbo.Questions WHERE ParentId IS NOT NULL ORDER BY Sequence;";

        public string Insert() => "INSERT INTO dbo.Questions (Name, Name_ZH, Name_FR, Name_IT, Name_PT, Name_RU, Name_ES, Name_UR, Name_HI, Name_MR, HtmlText, FormInputId, Sequence, CategoryId, IsRequired, IsVisible) " +
                                  "VALUES (@Name, @Name_ZH, @Name_FR, @Name_IT, @Name_PT, @Name_RU, @Name_ES, @Name_UR, @Name_HI, @Name_MR, @HtmlText, @FormInputId, @Sequence, @CategoryId, @IsRequired, @IsVisible);" +
                                   "SELECT CAST(SCOPE_IDENTITY() AS INT);";

        public string Update(string Language)
        {
            if (Language == "admin")
            {
                return "UPDATE dbo.Questions SET " +
                  "Name = @Name, " +
                  "Name_ZH = @Name_ZH, " +
                  "Name_FR = @Name_FR, " +
                  "Name_IT = @Name_IT, " +
                  "Name_PT = @Name_PT, " +
                  "Name_RU = @Name_RU, " +
                  "Name_ES = @Name_ES, " +
                  "Name_UR = @Name_UR, " +
                  "Name_HI = @Name_HI, " +
                  "Name_MR = @Name_MR, " +
                  "HtmlText = @HtmlText, " +
                  "FormInputId = @FormInputId," +
                  "Sequence = @Sequence," +
                  "IsVisible = @IsVisible," +
                  "CategoryId = @CategoryId, " +
                  "IsRequired = @IsRequired " +
                  "WHERE Id = @Id;";
            }
            else if (Language == "en")
            {
                return "UPDATE dbo.Questions SET Name = @Name WHERE Id = @Id;";
            }
            else
            {
                return "UPDATE dbo.Questions SET Name_" + Language.ToUpper() + " = @Name_" + Language.ToUpper() + " WHERE Id = @Id;";
            }
        }
    }

    public interface IQuestionQuery
    {
        string Insert();
        string Update(string Language);
        string FindById();
        string GetAll();
        string GetAllChild();
        string Delete();
        string Details();
    }
}
