﻿namespace VaccineSurvey.Data.Queries
{
    public class OptionQuery : IOptionQuery
    {
        public string Delete() => "DELETE FROM dbo.QuestionOptions WHERE Id = @Id;";

        public string FindById() => "SELECT TOP(1) qo.* FROM dbo.QuestionOptions qo WHERE Id = @Id;";

        public string Insert() => "INSERT INTO dbo.QuestionOptions (QuestionId, Name, Name_ZH, Name_FR, Name_IT, Name_PT, Name_RU, Name_ES, Name_UR, Name_HI, Name_MR, Sequence, ColumnWidth, IsVisible) " +
                                  "VALUES (@QuestionId, @Name, @Name_ZH, @Name_FR, @Name_IT, @Name_PT, @Name_RU, @Name_ES, @Name_UR, @Name_HI, @Name_MR, @Sequence, @ColumnWidth, @IsVisible);";

        public string GetAll() => "SELECT * FROM dbo.QuestionOptions ORDER BY Sequence;";

        public string Update(string Language)
        {
            if (Language == "admin")
            {
                return "UPDATE dbo.QuestionOptions SET " +
                                  "Name = @Name, " +
                                  "Name_ZH = @Name_ZH, " +
                                  "Name_FR = @Name_FR, " +
                                  "Name_IT = @Name_IT, " +
                                  "Name_PT = @Name_PT, " +
                                  "Name_RU = @Name_RU, " +
                                  "Name_ES = @Name_ES, " +
                                  "Name_UR = @Name_UR, " +
                                  "Name_HI = @Name_HI, " +
                                  "Name_MR = @Name_MR, " +
                                  "Sequence = @Sequence, " +
                                  "ColumnWidth = @ColumnWidth, " +
                                  "IsVisible = @IsVisible " +
                                  "WHERE Id = @Id;";
            }
            else if (Language == "en")
            {
                return "UPDATE dbo.QuestionOptions SET Name = @Name WHERE Id = @Id;";
            }
            else
            {
                return "UPDATE dbo.QuestionOptions SET Name_" + Language.ToUpper() + " = @Name_" + Language.ToUpper() + " WHERE Id = @Id;";
            }
        } 
    }

    public interface IOptionQuery
    {
        string Insert();
        string Update(string Language);
        string Delete();
        string FindById();
        string GetAll();
    }
}
