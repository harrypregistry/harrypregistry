﻿namespace VaccineSurvey.Data.Queries
{
    public class CategoryQuery : ICategoryQuery
    {
        public string Insert() => "INSERT INTO dbo.Categories (Name, Name_ZH, Name_FR, Name_IT, Name_PT, Name_RU, Name_ES, Name_UR, Name_HI, Name_MR, Sequence, IsVisible) " +
                                  "VALUES (@Name, @Name_ZH, @Name_FR, @Name_IT, @Name_PT, @Name_RU, @Name_ES, @Name_UR, @Name_HI, @Name_MR, @Sequence, @IsVisible);";

        public string Update(string Language)
        {
            if (Language == "admin")
            {
                return "UPDATE dbo.Categories SET " +
                                  "Name = @Name, " +
                                  "Name_ZH = @Name_ZH, " +
                                  "Name_FR = @Name_FR, " + 
                                  "Name_IT = @Name_IT, " + 
                                  "Name_PT = @Name_PT, " +
                                  "Name_RU = @Name_RU, " +
                                  "Name_ES = @Name_ES, " +
                                  "Name_UR = @Name_UR, " +
                                  "Name_HI = @Name_HI, " +
                                  "Name_MR = @Name_MR, " +
                                  "Sequence = @Sequence, " +
                                  "IsVisible = @IsVisible " +
                                  "WHERE Id = @Id;";
            }
            else if (Language == "en")
            {
                return "UPDATE dbo.Categories SET Name = @Name WHERE Id = @Id;";
            }
            else
            {
                return "UPDATE dbo.Categories SET Name_" + Language.ToUpper() + " = @Name_" + Language.ToUpper() + " WHERE Id = @Id;";
            }
        }

        public string FindById() => "SELECT TOP(1) c.* FROM dbo.Categories c WHERE Id = @Id;";

        public string DeleteProcedure() => "DELETE FROM dbo.QuestionSubmits WHERE CategoryId = @CategoryId; " +
                "DELETE qo FROM dbo.QuestionOptions qo INNER JOIN dbo.Questions q ON qo.QuestionId = q.Id WHERE q.CategoryId = @CategoryId; " +
                "DELETE FROM dbo.Questions WHERE CategoryId = @CategoryId; " +
                "DELETE FROM dbo.Categories WHERE Id = @CategoryId; ";

        public string GetAll() => "SELECT * FROM dbo.Categories ORDER BY Sequence;";

        public string Details() => "SELECT TOP(1) c.* FROM dbo.Categories c WHERE Id = @Id;" +
                "SELECT * FROM dbo.Questions WHERE CategoryId = @Id AND ParentId IS NULL ORDER BY Sequence;" +
                "SELECT qo.* FROM dbo.QuestionOptions qo INNER JOIN " +
                "dbo.Questions q ON qo.QuestionId = q.Id " +
                "WHERE q.CategoryId = @Id AND q.ParentId IS NULL;";

        public string GetQuestions() => "SELECT * FROM dbo.Questions WHERE CategoryId = @Id AND ParentId IS NULL ORDER BY Sequence;";
    }

    public interface ICategoryQuery
    {
        string Insert();

        string Update(string Language);
        string FindById();
        string Details();

        string DeleteProcedure();
        string GetAll();
        string GetQuestions();
    }
}
