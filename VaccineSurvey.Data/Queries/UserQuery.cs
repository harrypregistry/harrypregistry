﻿namespace VaccineSurvey.Data.Queries
{ 
    public class UserQuery : IUserQuery
    {
        public string Delete() => "DELETE FROM dbo.AspNetUsers WHERE Id = @Id;";

        public string FindById() => "SELECT TOP(1) q.* FROM dbo.AspNetUsers q WHERE Id = @Id;";

        public string GetAll() => "SELECT * FROM dbo.AspNetUsers;";
    }

    public interface IUserQuery
    {  
        string FindById();
        string GetAll();
        string Delete(); 
    }
}