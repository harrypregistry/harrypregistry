﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Data.Queries;
using VaccineSurvey.Model;

namespace VaccineSurvey.Data.Repositories
{
    public class AuthRepository : DB, IAuthRepository
    {
        private readonly IUserQuery userQuery;
        public AuthRepository(IUserQuery userQuery)
        {
            this.userQuery = userQuery;
        }

        public async Task<List<ApplicationUser>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = userQuery.GetAll();
                var result = await conn.QueryAsync<ApplicationUser>(sqlQuery);
                return result.ToList();
            }
        }

        public async Task<ApplicationUser> FindByIdAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = userQuery.FindById();
                var result = await conn.QueryAsync<ApplicationUser>(sqlQuery, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task<bool> DeleteAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = userQuery.Delete();
                return await conn.ExecuteAsync(sqlQuery, new { Id = id }) > 0;
            }
        }
    }

    public interface IAuthRepository
    {
        Task<ApplicationUser> FindByIdAsync(object id);
        Task<bool> DeleteAsync(object id);
        Task<List<ApplicationUser>> GetAllAsync();
    }
}
