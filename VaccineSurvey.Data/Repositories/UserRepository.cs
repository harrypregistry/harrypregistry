﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VaccineSurvey.Data.DBConnections;

namespace VaccineSurvey.Data.Repositories
{
    public class UserRepository : DB, IUserRepository
    {
        public async Task<string> GetRolesAsync(string userId)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT		TOP(1) R.[Language] " +
                                  "FROM			dbo.AspNetUsers AS U " +
                                  "INNER JOIN	dbo.AspNetUserRoles AS UR ON U.Id = UR.UserId " +
                                  "INNER JOIN	dbo.AspNetRoles AS R ON UR.RoleId = R.Id " +
                                  "WHERE	    U.Id = @Id;";
                var results = await conn.QueryAsync<string>(sqlQuery, new { Id = userId });
                return results.Count() > 0 ? results.FirstOrDefault() : "";
            }
        }
    }

    public interface IUserRepository
    {
        Task<string> GetRolesAsync(string userId);
    }
}
