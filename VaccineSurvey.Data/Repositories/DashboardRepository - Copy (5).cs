﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Dto;
using VaccineSurvey.Model;
using VaccineSurvey.ViewModel;

namespace VaccineSurvey.Data.Repositories
{
    public class DashboardRepository : DB, IDashboardRepository
    {
        public DashboardRepository() { }

        public async Task<DashboardDto> GetAsync()
        {
            var mgr = new DashboardDto();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT * FROM dbo.Categories WHERE IsVisible = 1 ORDER BY Sequence;" +
                                  "SELECT * FROM dbo.Questions WHERE IsVisible = 1 AND ParentId = 0 OR ParentId IS NULL ORDER BY Sequence;" +
                                  "SELECT * FROM dbo.Questions WHERE IsVisible = 1 AND ParentId != 0 AND ParentId IS NOT NULL ORDER BY Sequence;" +
                                  "SELECT * FROM dbo.QuestionOptions WHERE IsVisible = 1 ORDER BY Sequence";
                using (var multi = await conn.QueryMultipleAsync(sqlQuery))
                {
                    mgr.Categories = multi.Read<Category>().ToList();
                    mgr.Questions = multi.Read<Question>().ToList();
                    mgr.ChildQuestions = multi.Read<Question>().ToList();
                    mgr.QuestionOptions = multi.Read<QuestionOption>().ToList();
                }
            }
            return mgr;
        }

        public async Task<ExcelDto> GetExcelAsync(DateTime DateFrom, DateTime DateTo)
        {
            var mgr = new ExcelDto();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = @"SELECT t.Id, t.OptionId, t.ColumnName 
                                    FROM (SELECT        q.CategoryId, 
                                                        q.Sequence,
		                                                CASE 
			                                                WHEN q.Id = 11 THEN q.Id 
			                                                WHEN ISNULL(c.Name, '') = '' THEN q.Id 
			                                                ELSE ISNULL(c.Id, 0) 
		                                                END AS Id, 
		                                                ISNULL(o.Id, 0) AS OptionId, 
		                                                CASE 
			                                                WHEN q.Id = 11 THEN q.Name 
			                                                WHEN q.FormInputId = 3 THEN q.Name + ' [' + ISNULL(o.Name, '') + ']' 
			                                                WHEN ISNULL(c.Name,'') = '' THEN q.Name 
			                                                ELSE q.Name + ' [' + ISNULL(c.Name, '') + ']' 
		                                                END AS ColumnName 
		                                    FROM        dbo.Questions AS q 
		                                    LEFT JOIN   dbo.QuestionOptions AS o ON o.QuestionId = q.Id AND q.FormInputId = 3 
		                                    LEFT JOIN   dbo.Questions AS c ON ISNULL(c.ParentId, 0) = q.Id AND  ISNULL(c.ParentId, 0) != 0   AND c.Id != 24  AND c.Id != 78
		                                    WHERE       ISNULL(q.ParentId, 0) = 0    
 
		                                    UNION 

		                                    SELECT      q.CategoryId, 
                                                        q.Sequence,
		                                                q.Id AS Id, 
		                                                0 AS OptionId, 
		                                                q.Name AS ColumnName  
		                                    FROM        dbo.Questions AS q    
		                                    WHERE       ISNULL(q.ParentId, 0) = 0  
		                                    AND			q.Id != 11 
		                                    AND			q.FormInputId NOT IN (3, 5)  

		                                    UNION
		
		                                    SELECT       c1.CategoryId, 
                                                         c1.Sequence,
		                                                 c1.Id AS Id, 
		                                                 0 AS OptionId, 
		                                                 q.Name + ' [' + ISNULL(c.Name, '') + ']' + ' [' + ISNULL(c1.Name, '') + ']' AS ColumnName  
		                                    FROM         dbo.Questions AS q 
		                                    Inner JOIN   dbo.Questions AS c ON ISNULL(c.ParentId, 0) = q.Id AND  ISNULL(c.ParentId, 0) != 0  
		                                    Inner JOIN   dbo.Questions AS c1 ON ISNULL(c1.ParentId, 0) = c.Id AND  ISNULL(c1.ParentId, 0) != 0    
		                                    WHERE        q.Id != 11   

		                                    UNION
		
		                                    SELECT       q.CategoryId, q.Sequence,
		                                                 ISNULL(c.Id, 0)  AS Id, 
		                                                 ISNULL(o.Id, 0) AS OptionId, 
		                                                 q.Name + ' [' + ISNULL(c.Name, '') + ']' + ' [' + ISNULL(o.Name, '') + ']' AS ColumnName   
		                                    FROM         dbo.Questions AS q   
		                                    Inner JOIN   dbo.Questions AS c ON ISNULL(c.ParentId, 0) = q.Id AND  ISNULL(c.ParentId, 0) != 0  
		                                    Inner JOIN   dbo.QuestionOptions AS o ON o.QuestionId = c.Id AND c.FormInputId = 3    
		                                    WHERE        q.Id != 11    
		 
	                                    ) AS t
                                    ORDER BY    t.CategoryId, t.Sequence; 


                                    SELECT		qs.Id, qs.CurrentUserId, qs.CategoryId, qs.QuestionId, qs.QuestionOptionId, 
                                    CASE 
                                        WHEN qs.QuestionId IN(16, 33, 35, 37) THEN 
												                                    CASE 
													                                    WHEN qs.Answer IS NULL THEN qo.Name  
													                                    ELSE qo.Name + '->' + ISNULL(qs.Answer, '')  
												                                    END   
                                        WHEN qs.QuestionOptionId IS NULL THEN qs.Answer 
                                        WHEN qo.Name LIKE 'Other%' THEN REPLACE(ISNULL(qs.Answer, ''), qo.Name + ':', '')
                                        ELSE qo.Name 
                                    END AS Answer, CreatedDate  
                                    FROM        QuestionSubmits AS qs 
                                    INNER JOIN  Categories AS c ON c.Id = qs.CategoryId 
                                    LEFT JOIN   QuestionOptions AS qo ON qo.Id = qs.QuestionOptionId 
                                    WHERE       CONVERT(DATE, CreatedDate) BETWEEN @DateFrom AND @DateTo 
                                    ORDER BY CreatedDate DESC, CurrentUserId; 

                                    SELECT DISTINCT CurrentUserId, CreatedDate FROM QuestionSubmits 
                                    WHERE CONVERT(DATE, CreatedDate) BETWEEN @DateFrom AND @DateTo 
                                    ORDER BY CreatedDate DESC, CurrentUserId;";

                var param = new DynamicParameters();
                param.Add("@DateFrom", DateFrom);
                param.Add("@DateTo", DateTo);

                using (var multi = await conn.QueryMultipleAsync(sqlQuery, param, commandTimeout: 6000))
                {
                    mgr.ExcelQuestions = multi.Read<ExcelQuestion>().ToList();
                    mgr.QuestionSubmit = multi.Read<QuestionSubmit>().ToList();
                    mgr.Users = multi.Read<QuestionSubmit>().ToList();
                }
            }
            return mgr;
        }

        public async Task<List<ResultDto>> GetResults(string UserId)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var param = new DynamicParameters();
                param.Add("@CurrentUserId", UserId);

                var sqlQuery = "SELECT		CurrentUserId, qs.QuestionId, c.Name AS CategoryName, q.Name AS QuestionText,  " +
                                "CASE " +
                                "       WHEN qs.QuestionId IN(16, 33, 35, 37) THEN qo.Name + ' ' + ISNULL(qs.Answer, '') " +
                                "       WHEN qs.QuestionOptionId IS NULL THEN qs.Answer  " +
                                "       WHEN qo.Name LIKE 'Other%' THEN ISNULL(qs.Answer, '')  " +
                                "       ELSE qo.Name " +
                                "END AS Answer, CreatedDate " +
                                "FROM           QuestionSubmits AS qs " +
                                "INNER JOIN     Categories AS c ON c.Id = qs.CategoryId " +
                                "INNER JOIN     Questions AS q ON q.Id = qs.QuestionId " +
                                "LEFT JOIN      QuestionOptions AS qo ON qo.Id = qs.QuestionOptionId " +
                                "WHERE          CurrentUserId = @CurrentUserId " +
                                "ORDER BY       CreatedDate DESC, CurrentUserId, qs.CategoryId, qs.QuestionId;";

                var result = await conn.QueryAsync<ResultDto>(sqlQuery, param);
                return result.ToList();
            }
        }

        public async Task<List<QuestionSubmit>> GetResultsUserList(DateTime DateFrom, DateTime DateTo)
        {
            var mgr = new List<QuestionSubmit>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var sqlQuery = "SELECT DISTINCT CurrentUserId, CreatedDate FROM QuestionSubmits " +
                    "WHERE CONVERT(DATE, CreatedDate) BETWEEN @DateFrom AND @DateTo " +
                    "ORDER BY CreatedDate DESC, CurrentUserId;";

                var param = new DynamicParameters();
                param.Add("@DateFrom", DateFrom);
                param.Add("@DateTo", DateTo);

                var result = await conn.QueryAsync<QuestionSubmit>(sqlQuery, param);
                mgr = result.ToList();
            }
            return mgr;
        }

        public async Task<List<QuestionSubmit>> GetResultsUserList()
        {
            var mgr = new List<QuestionSubmit>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var sqlQuery = "SELECT  DISTINCT qs .CurrentUserId, qs .CreatedDate, qo.Name AS Answer " +
                                "FROM dbo.QuestionSubmits AS qs " +
                                "INNER JOIN dbo.QuestionOptions AS qo ON qo.Id = qs.QuestionOptionId " +
                                "WHERE qs.QuestionId = 2 ORDER BY CreatedDate DESC, CurrentUserId;";

                var result = await conn.QueryAsync<QuestionSubmit>(sqlQuery);
                mgr = result.ToList();
            }
            return mgr;
        }

        public async Task<GraphsDto> GetGraphData()
        {
            var mgr = new GraphsDto();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT COUNT(DISTINCT CurrentUserId) FROM dbo.QuestionSubmits;" + //TotalSubmitions
                                  "SELECT QuestionOptionId, COUNT(*) AS CountryCount FROM dbo.QuestionSubmits WHERE QuestionId = 2 GROUP BY QuestionOptionId;" + //Country Count
                                  "SELECT * FROM dbo.Questions WHERE Id IN (84, 105, 85, 42, 48, 112);" + //Questions
                                  "SELECT * FROM dbo.Questions WHERE Id IN (62, 113, 87, 109, 90);" + //ChildQuestions
                                  "SELECT * FROM dbo.QuestionOptions WHERE QuestionId IN (2, 84, 105, 85, 42, 48, 112);" + //QuestionOptions
                                  "SELECT       qs.* " +
                                  "FROM         dbo.QuestionSubmits AS qs " +
                                  "INNER JOIN   dbo.Questions AS q ON q.Id = qs.QuestionId " +
                                  "WHERE        q.Id IN (87, 109, 90, 42, 62, 113);" +

                                  "select * from QuestionSubmits where QuestionId = 42 and CurrentUserId in( SELECT  [CurrentUserId]       FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =6)" +

                                  "select * from QuestionSubmits where QuestionId = 42 and CurrentUserId in( SELECT  [CurrentUserId]       FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =7)" +
                                  "select * from QuestionSubmits where QuestionId = 90 and CurrentUserId in( SELECT  [CurrentUserId]       FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =6)" +

                                  "select * from QuestionSubmits where QuestionId = 90 and CurrentUserId in( SELECT  [CurrentUserId]       FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =7)" +

                                   "SELECT  count(*)    as NoCount   FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =7" +
                                    "SELECT  count(*)  as NoCount      FROM [dbo].[QuestionSubmits] where QuestionId = 8 AND QuestionOptionId =6";

                using (var multi = await conn.QueryMultipleAsync(sqlQuery, commandTimeout: 180))
                {
                    mgr.TotalNumberOfSubmission = multi.Read<int>().FirstOrDefault();





                    var CountryCounts = multi.Read<CountryCounts>().ToList();
                    mgr.Questions = multi.Read<Question>().ToList();



                    mgr.ChildQuestions = multi.Read<Question>().ToList();
                    mgr.QuestionOptions = multi.Read<QuestionOption>().ToList();
                    mgr.QuestionSubmit = multi.Read<QuestionSubmit>().ToList();
                    mgr.PregnantUser42 = multi.Read<QuestionSubmit>().ToList();
                    mgr.NonPregnantUser42 = multi.Read<QuestionSubmit>().ToList();

                    mgr.PregnantUser85 = multi.Read<QuestionSubmit>().ToList();
                    mgr.NonPregnantUser85 = multi.Read<QuestionSubmit>().ToList();
                    mgr.NoCount = multi.Read<int>().FirstOrDefault();
                    mgr.YesCount = multi.Read<int>().FirstOrDefault();




                    foreach (var item in CountryCounts)
                    {
                        item.Name = mgr.QuestionOptions.Where(x => x.Id == item.QuestionOptionId).FirstOrDefault().Name;

                    }

                    var lst = CountryCounts.Where(x => x.Name == "Scotland" || x.Name == "Wales" || x.Name == "Northern Ireland" || x.Name == "England" || x.Name == "United Kingdom");
                    int count = 0;
                    foreach (var item in lst.OrderByDescending(x => x.QuestionOptionId))
                    {
                        count += item.CountryCount;


                        if (item.Name == "United Kingdom")
                        {
                            item.CountryCount = count;
                        }
                    }

                    foreach (var item in lst.ToList())
                    {
                        if (item.Name != "United Kingdom")
                        {
                            CountryCounts.Remove(item);
                        }
                    }

                    var newCountryLst = CountryCounts.Where(x => x.Name != "Scotland" || x.Name != "Wales" || x.Name != "Northern Ireland" || x.Name != "England");

                    mgr.CountryCounts = newCountryLst.ToList();




                }
            }
            return mgr;
        }

        public async Task<List<PageData>> GetPageDataAll()
        {
            var mgr = new List<PageData>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT * FROM dbo.PageDatas;";
                var result = await conn.QueryAsync<PageData>(sqlQuery);
                mgr = result.ToList();
            }
            return mgr;
        }

        public async Task<PageData> GetPageDataByName(string PageName)
        {
            var mgr = new PageData();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT TOP(1) q.* FROM dbo.PageDatas q WHERE PageName = @PageName;";
                var param = new DynamicParameters();
                param.Add("@PageName", PageName);
                var result = await conn.QueryAsync<PageData>(sqlQuery, param);
                mgr = result.FirstOrDefault();
            }
            return mgr;
        }

        public async Task<List<CountryViewModel>> GetCountryCount()
        {
            var mgr = new List<CountryViewModel>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT DISTINCT qo.Name AS Country, COUNT(*) AS Counter " +
                                "FROM dbo.QuestionSubmits AS qs " +
                                "INNER JOIN dbo.QuestionOptions AS qo ON qo.Id = qs.QuestionOptionId " +
                                "WHERE qs.QuestionId = 2 " +
                                "GROUP BY qo.Name " +
                                "HAVING  COUNT(*) > 0 " +
                                "ORDER BY qo.Name";
                var result = await conn.QueryAsync<CountryViewModel>(sqlQuery, commandTimeout: 6400);
                mgr = result.ToList();
            }
            return mgr;
        }

        public async Task<List<CountryFeedbackViewModel>> GetCountryFeedback()
        {
            var mgr = new List<CountryFeedbackViewModel>();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT		(select Name from QuestionOptions where id = A.QuestionOptionId) AS Country, A.CurrentUserId AS CurrentUserId, B.Answer AS Feedback, FORMAT (A.CreatedDate, 'dd-MM-yy hh:mm tt') as FeedbackDate " +
                                  "FROM         QuestionSubmits AS A " +
                                  "INNER JOIN   QuestionSubmits AS B ON(A.CurrentUserId = B.CurrentUserId AND B.QuestionId = 44) " +
                                  "WHERE        A.QuestionId = 2 " +
                                  "ORDER BY     A.CreatedDate desc";
                var result = await conn.QueryAsync<CountryFeedbackViewModel>(sqlQuery, commandTimeout: 6400);
                mgr = result.ToList();
            }
            return mgr;
        }
    }

    public interface IDashboardRepository
    {
        Task<List<CountryViewModel>> GetCountryCount();
        Task<List<CountryFeedbackViewModel>> GetCountryFeedback();
        Task<DashboardDto> GetAsync();
        Task<ExcelDto> GetExcelAsync(DateTime DateFrom, DateTime DateTo);
        Task<List<ResultDto>> GetResults(string UserId);
        Task<List<QuestionSubmit>> GetResultsUserList();
        Task<List<QuestionSubmit>> GetResultsUserList(DateTime DateFrom, DateTime DateTo);
        Task<GraphsDto> GetGraphData();
        Task<List<PageData>> GetPageDataAll();
        Task<PageData> GetPageDataByName(string PageName);
    }
}
