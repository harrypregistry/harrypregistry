﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Data.Queries;
using VaccineSurvey.Model;

namespace VaccineSurvey.Data.Repositories
{
    public class OptionRepository : DB, IOptionRepository
    {
        private readonly IOptionQuery optionQuery;
        public OptionRepository(IOptionQuery optionQuery)
        {
            this.optionQuery = optionQuery;
        }

        public async Task<List<QuestionOption>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = optionQuery.GetAll();
                var result = await conn.QueryAsync<QuestionOption>(sqlQuery);
                return result.ToList();
            }
        }
        public async Task<bool> CreateAsync(QuestionOption entity)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = optionQuery.Insert();
                return await conn.ExecuteAsync(sqlQuery, entity) > 0;
            }
        }

        public async Task<bool> DeleteAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = optionQuery.Delete();
                return await conn.ExecuteAsync(sqlQuery, new { Id = id }) > 0;
            }
        }

        public async Task<QuestionOption> FindByIdAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = optionQuery.FindById();
                var result = await conn.QueryAsync<QuestionOption>(sqlQuery, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task<bool> UpdateAsync(QuestionOption entity, string Language)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = optionQuery.Update(Language);
                return await conn.ExecuteAsync(sqlQuery, entity) > 0;
            }
        }
    }

    public interface IOptionRepository
    {
        Task<List<QuestionOption>> GetAllAsync();
        Task<bool> CreateAsync(QuestionOption entity);
        Task<bool> UpdateAsync(QuestionOption entity, string Language);
        Task<QuestionOption> FindByIdAsync(object id);
        Task<bool> DeleteAsync(object id);
    }
}
