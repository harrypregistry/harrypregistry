﻿using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Dto;

namespace VaccineSurvey.Data.Repositories
{
    public class FormRepository : DB, IFormRepository
    {
        public FormRepository() { }

        public async Task<bool> Save(List<QuestionSubmitDto> model)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "INSERT INTO dbo.QuestionSubmits (CurrentUserId, CategoryId, QuestionId, QuestionOptionId, CreatedDate, Answer, StartTime, EndTime) " +
                                               "VALUES(@CurrentUserId, @CategoryId, @QuestionId, @QuestionOptionId, @CreatedDate, @Answer, @StartTime, @EndTime);";
                var affectedRows = await conn.ExecuteAsync(sqlQuery, model);
                if (affectedRows > 0) return true;
            }
            return false;
        }
    }

    public interface IFormRepository
    {
        Task<bool> Save(List<QuestionSubmitDto> model);
    }
}
