﻿using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaccineSurvey.Data.Repositories
{
    public class ReplaceTextRepository : DB, IReplaceTextRepository
    {
        public async Task<IEnumerable<ReplaceText>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var result = await conn.QueryAsync<ReplaceText>("SELECT * FROM dbo.ReplaceTexts ORDER BY Id DESC;");
                return result.ToList();
            }
        }

        public async Task<bool> CreateAsync(ReplaceText entity, string roleName)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var param = new DynamicParameters();
                param.Add("@FindText", entity.FindText);
                param.Add("@ReplaceWith", entity.ReplaceWith);
                param.Add("@Lang", roleName);
                var Id = await conn.ExecuteAsync("[dbo].[sp_FindReplaceText]", param, commandType: CommandType.StoredProcedure);
                return Id > 0;
            }
        }

        public async Task<bool> QuestionsAsync(ReplaceText entity, string roleName)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var param = new DynamicParameters();
                param.Add("@FindText", entity.FindText);
                param.Add("@ReplaceWith", entity.ReplaceWith);
                param.Add("@Lang", roleName);
                var Id = await conn.ExecuteAsync("[dbo].[sp_FindReplaceText]", param, commandType: CommandType.StoredProcedure);
                return Id > 0;
            }
        }

        public async Task<bool> OptionsAsync(ReplaceText entity, string roleName)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                var param = new DynamicParameters();
                param.Add("@FindText", entity.FindText);
                param.Add("@ReplaceWith", entity.ReplaceWith);
                param.Add("@Lang", roleName);
                var Id = await conn.ExecuteAsync("[dbo].[sp_FindReplaceOptionText]", param, commandType: CommandType.StoredProcedure);
                return Id > 0;
            }
        }
    }

    public interface IReplaceTextRepository
    {
        Task<IEnumerable<ReplaceText>> GetAllAsync();
        Task<bool> CreateAsync(ReplaceText entity, string roleName);
        Task<bool> QuestionsAsync(ReplaceText entity, string roleName);
        Task<bool> OptionsAsync(ReplaceText entity, string roleName);
    }
}