﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Data.Queries;
using VaccineSurvey.Model;
using VaccineSurvey.ViewModel;

namespace VaccineSurvey.Data.Repositories
{
    public class CategoryRepository : DB, ICategoryRepository
    {
        private readonly ICategoryQuery categoryQuery;

        public CategoryRepository(ICategoryQuery categoryQuery) => this.categoryQuery = categoryQuery;

        public async Task<bool> CreateAsync(Category entity)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.Insert();
                return await conn.ExecuteAsync(sqlQuery, entity) > 0;
            }
        }

        public async Task<bool> UpdateAsync(Category entity, string Language)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.Update(Language);
                return await conn.ExecuteAsync(sqlQuery, entity) > 0;
            }
        }

        public async Task<Category> FindByIdAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.FindById();
                var result = await conn.QueryAsync<Category>(sqlQuery, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task<bool> DeleteAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.DeleteProcedure();
                var param = new DynamicParameters();
                param.Add("@CategoryId", id);
                return await conn.ExecuteAsync(sqlQuery, param) > 0;
            }
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.GetAll();
                var items = await conn.QueryAsync<Category>(sqlQuery);
                return items.ToList();
            }
        }

        public async Task<CategoryDetailViewModel> DetailsAsync(object id)
        {
            var viewModel = new CategoryDetailViewModel();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.Details();
                using (var multi = await conn.QueryMultipleAsync(sqlQuery, new { Id = id }))
                {
                    viewModel.Category = multi.Read<Category>().FirstOrDefault();
                    if (viewModel.Category == null)
                    {
                        return null;
                    }
                    viewModel.Questions = multi.Read<Question>().ToList();
                    viewModel.Options = multi.Read<QuestionOption>().ToList();
                }
            }
            return viewModel;
        }

        public async Task<IEnumerable<Question>> GetQuestionsAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = categoryQuery.GetQuestions();
                var items = await conn.QueryAsync<Question>(sqlQuery, new { Id = id });
                return items.ToList();
            }
        }

        public async Task<long> GetByNameAsync(string name)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT TOP(1) c.Id FROM dbo.Categories c WHERE Name = @Name;";
                var resultCatgory = await conn.QueryAsync<Category>(sqlQuery, new { Name = name });
                var category = resultCatgory.FirstOrDefault();
                return category != null ? category.Id : 0;
            }
        }
    }

    public interface ICategoryRepository
    {
        Task<bool> CreateAsync(Category entity);
        Task<bool> UpdateAsync(Category entity, string Language);
        Task<Category> FindByIdAsync(object id);
        Task<bool> DeleteAsync(object id);
        Task<IEnumerable<Category>> GetAllAsync();
        Task<CategoryDetailViewModel> DetailsAsync(object id);
        Task<IEnumerable<Question>> GetQuestionsAsync(object id);
        Task<long> GetByNameAsync(string name);

    }
}