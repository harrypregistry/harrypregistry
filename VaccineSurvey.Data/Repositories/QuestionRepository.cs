﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Data.Queries;
using VaccineSurvey.Model;
using VaccineSurvey.ViewModel;

namespace VaccineSurvey.Data.Repositories
{
    public class QuestionRepository : DB, IQuestionRepository
    {
        private readonly IQuestionQuery questionQuery;
        public QuestionRepository(IQuestionQuery questionQuery)
        {
            this.questionQuery = questionQuery;
        }

        public async Task<int> CreateAsync(Question entity)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.Insert();
                var Id = await conn.ExecuteScalarAsync<int>(sqlQuery, entity);
                return Id;
            }
        }

        public async Task<List<Question>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.GetAll();
                var result = await conn.QueryAsync<Question>(sqlQuery);
                return result.ToList();
            }
        }

        public async Task<List<Question>> GetChildQuestion()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.GetAllChild();
                var result = await conn.QueryAsync<Question>(sqlQuery);
                return result.ToList();
            }
        }

        public async Task<Question> FindByIdAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.FindById();
                var result = await conn.QueryAsync<Question>(sqlQuery, new { Id = id });
                return result.FirstOrDefault();
            }
        }

        public async Task<bool> UpdateAsync(Question entity, string Language)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.Update(Language);
                return await conn.ExecuteAsync(sqlQuery, entity) > 0;
            }
        }

        public async Task<bool> DeleteAsync(object id)
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "DELETE FROM dbo.QuestionSubmits WHERE QuestionId = @QuestionId;";
                await conn.ExecuteAsync(sqlQuery, new { QuestionId = id }); 
                sqlQuery = "DELETE FROM dbo.QuestionOptions WHERE QuestionId = @QuestionId;";
                await conn.ExecuteAsync(sqlQuery, new { QuestionId = id });
                sqlQuery = "UPDATE dbo.Questions SET ParentId = NULL WHERE ParentId = @QuestionId;";
                await conn.ExecuteAsync(sqlQuery, new { QuestionId = id });
                sqlQuery = questionQuery.Delete();
                return await conn.ExecuteAsync(sqlQuery, new { Id = id }) > 0;
            }
        }

        public async Task<QuestionDetailViewModel> DetailsAsync(object id)
        {
            var viewModel = new QuestionDetailViewModel();
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = questionQuery.Details();
                using (var multi = await conn.QueryMultipleAsync(sqlQuery, new { Id = id }))
                {
                    viewModel.Question = multi.Read<Question>().FirstOrDefault();
                    if (viewModel.Question == null)
                    {
                        return null;
                    }
                    viewModel.Options = multi.Read<QuestionOption>().ToList();
                }
            }
            return viewModel;
        }

    }

    public interface IQuestionRepository
    {
        Task<int> CreateAsync(Question entity);
        Task<bool> UpdateAsync(Question entity, string Language);
        Task<Question> FindByIdAsync(object id);
        Task<bool> DeleteAsync(object id);
        Task<QuestionDetailViewModel> DetailsAsync(object id);
        Task<List<Question>> GetAllAsync();
        Task<List<Question>> GetChildQuestion();
    }
}
