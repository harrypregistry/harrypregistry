﻿using Dapper;
using VaccineSurvey.Data.DBConnections;
using VaccineSurvey.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VaccineSurvey.Data.Repositories
{
    public class FormInputRepository :DB, IFormInputRepository
    {
        public async Task<IEnumerable<FormInput>> GetAllAsync()
        {
            using (IDbConnection conn = Connection)
            {
                conn.Open();
                string sqlQuery = "SELECT * FROM dbo.FormInputs;";
                var result = await conn.QueryAsync<FormInput>(sqlQuery);
                return result.ToList();
            }
        }
    }

    public interface IFormInputRepository
    {
        Task<IEnumerable<FormInput>> GetAllAsync();
    }

}
