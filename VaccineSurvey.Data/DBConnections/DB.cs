﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace VaccineSurvey.Data.DBConnections
{
    public class DB
    {
        #region DB Connection
        protected IDbConnection Connection
        {
            get
            {
                return new SqlConnection(ConfigurationManager.ConnectionStrings["VaccineSurveyConnection"].ToString());
            }
        }
        #endregion
    }
}
