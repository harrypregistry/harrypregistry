﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.Dto
{
    public class ExcelDto
    {
        public ExcelDto()
        {
            Questions = new List<Question>();
            ExcelQuestions = new List<ExcelQuestion>();
            ChildQuestions = new List<Question>();
            QuestionSubmit = new List<QuestionSubmit>();
            Users = new List<QuestionSubmit>(); 
        }
        public List<Question> Questions { get; set; }
        public List<ExcelQuestion> ExcelQuestions { get; set; }
        public List<Question> ChildQuestions { get; set; }
        public List<QuestionSubmit> QuestionSubmit { get; set; }
        public List<QuestionSubmit> Users { get; set; } 

    }

    public class ExcelLoop
    {
        public long Id { get; set; }
        public string Column { get; set; } 
    }

    public class ExcelQuestion
    {
        public long Id { get; set; }
        public long OptionId { get; set; }
        public string ColumnName { get; set; }
    } 
}
