﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.Dto
{
    public class GraphsDto
    {
        public int TotalNumberOfSubmission { get; set; }
        public List<CountryCounts> CountryCounts { get; set; }
        public List<Question> Questions { get; set; }
        public List<Question> ChildQuestions { get; set; }
        public List<QuestionOption> QuestionOptions { get; set; }
        public List<QuestionSubmit> QuestionSubmit { get; set; }

        public List<QuestionSubmit> PregnantUser42 { get; set; }


        public List<QuestionSubmit> NonPregnantUser42 { get; set; }

        public List<QuestionSubmit> PregnantUser85 { get; set; }


        public List<QuestionSubmit> NonPregnantUser85 { get; set; }

        public int YesCount { get; set; }

        public int NoCount { get; set; }
    }
    public class CountryCounts
    {
        public long QuestionOptionId { get; set; }
        public int CountryCount { get; set; }

        public string Name { get; set; }
    }


}