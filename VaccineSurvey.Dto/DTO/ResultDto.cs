﻿using System;

namespace VaccineSurvey.Dto
{
    public class ResultDto
    {
        public string CurrentUserId { get; set; }
        public long QuestionId{ get; set; }
        public string CategoryName { get; set; }
        public string QuestionText { get; set; }
        public string Answer { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}