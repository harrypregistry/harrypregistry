﻿using System;

namespace VaccineSurvey.Dto
{
    public class QuestionSubmitDto
    {
        public QuestionSubmitDto()
        {
            Answer = String.Empty;
            CreatedDate = DateTime.UtcNow;
        }
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
        public long? QuestionOptionId { get; set; }
        public string CurrentUserId { get; set; }
        public string Answer { get; set; } 
        public DateTime CreatedDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
    }
}
