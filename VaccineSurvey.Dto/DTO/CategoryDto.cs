﻿namespace VaccineSurvey.Dto
{
    public class CategoryDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Paragraph { get; set; }
    }
}
