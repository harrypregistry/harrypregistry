﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.Dto
{
    public class DashboardDto
    {
        public DashboardDto()
        {
            Categories = new List<Category>();
            QuestionOptions = new List<QuestionOption>();
            Questions = new List<Question>();
            ChildQuestions = new List<Question>();
        }
        public List<Category> Categories { get; set; }
        public List<QuestionOption> QuestionOptions { get; set; }
        public List<Question> Questions { get; set; }
        public List<Question> ChildQuestions { get; set; }

    }
}
