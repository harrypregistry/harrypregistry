﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.ViewModel
{
    public class QuestionDetailViewModel
    {
        public QuestionDetailViewModel()
        {
            Options = new List<QuestionOption>();
            Questions = new List<Question>();
        }
        public Question Question { get; set; }
        public List<QuestionOption> Options { get; set; }
        public List<Question> Questions { get; set; }
        public List<Question> ChildQuestions { get; set; }
    }
}
