﻿namespace VaccineSurvey.ViewModel
{
    public class CountryViewModel
    {
        public string Country { get; set; }
        public int Counter { get; set; }
    }
}
