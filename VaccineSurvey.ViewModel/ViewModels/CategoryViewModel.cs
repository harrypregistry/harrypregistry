﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.ViewModel
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Category Name")]
        [Required] 
        public string CategoryName { get; set; }

        [Display(Name = "Category Name Português")]
        public string CategoryName_PT { get; set; }

        [Display(Name = "Category Name 漢語")]
        public string CategoryName_ZH { get; set; }

        [Display(Name = "Category Name Italiano")]
        public string CategoryName_IT { get; set; }

        [Display(Name = "Category Name Français")]
        public string CategoryName_FR { get; set; }

        [Display(Name = "Category Name Español")]
        public string CategoryName_ES { get; set; }

        [Display(Name = "Category Name Deutsch")]
        public string CategoryName_DE { get; set; }
         
        [Display(Name = "Category Name العربية")]
        public string CategoryName_AR { get; set; }
         
        [Display(Name = "Category Name русский")]
        public string CategoryName_RU { get; set; }
         
        [Display(Name = "Category Name 한국어")]
        public string CategoryName_KO { get; set; }

        [Required] 
        public int Sequence { get; set; }
    }
}