﻿using System.Collections.Generic;

namespace VaccineSurvey.ViewModel
{
    public class CountryFeedbackViewModel
    {
        public string Country { get; set; }
        public string CurrentUserId { get; set; }
        public string Feedback { get; set; }

        public string FeedbackDate { get; set; }
    }
    public class CountryFeedbackPaging
    {
        public List<CountryFeedbackViewModel> Result { get; set; }
        public PagingInfo PageInfo { get; set; }
    }
}
