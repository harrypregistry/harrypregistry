﻿namespace VaccineSurvey.ViewModel
{
    public class QuestionJsonViewModel
    {
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
        public long? QuestionOptionId { get; set; }
        public int FormInputId { get; set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
    }
}
