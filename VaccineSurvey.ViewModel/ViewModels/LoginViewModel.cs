﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.ViewModel
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string DateFormat { get; set; }

        [Display(Name = "Remember me?")]
        public bool? RememberMe { get; set; }
    }
}
