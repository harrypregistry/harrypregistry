﻿namespace VaccineSurvey.ViewModel
{
    public class ErrorMessageViewModel
    {
        public bool success { get; set; }
        public int ErrorType { get; set; }
        public string messageToClient { get; set; }
    }
}
