﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.ViewModel
{ 
    public class DashboardPaging
    {
        public List<QuestionSubmit> Result { get; set; }
        public PagingInfo PageInfo { get; set; }
    }
}
