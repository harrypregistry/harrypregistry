﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using VaccineSurvey.Model;

namespace VaccineSurvey.ViewModel
{
    public class QuestionViewModel
    {
        public QuestionViewModel() => ChildrenQuestions = new List<Question>();
        public int Id { get; set; }
        public int? ParentQuestionId { get; set; }

        [Display(Name = "Question")]
        public string QuestionText { get; set; }

        [Display(Name = "Question Português")]
        public string QuestionText_PT { get; set; }

        [Display(Name = "Question 漢語")]
        public string QuestionText_ZH { get; set; }

        [Display(Name = "Question Italiano")]
        public string QuestionText_IT { get; set; }

        [Display(Name = "Question Français")]
        public string QuestionText_FR { get; set; }

        [Display(Name = "Question Español")]
        public string QuestionText_ES { get; set; }

        [Display(Name = "Question Deutsch")]
        public string QuestionText_DE { get; set; }

        [Display(Name = "Question العربية")]
        public string QuestionText_AR { get; set; }

        [Display(Name = "Question русский")]
        public string QuestionText_RU { get; set; }

        [Display(Name = "Question 한국어")]
        public string QuestionText_KO { get; set; }

        [Display(Name = "Select Category")]
        public int CategoryId { get; set; }

        [Display(Name = "Form Input")]
        [Required]
        public int FormInputId { get; set; }

        [Display(Name = "Required")]
        public bool IsRequired { get; set; }
         
        [Required]
        public int Sequence { get; set; }

        public List<Question> ChildrenQuestions { get; set; }
    }
}