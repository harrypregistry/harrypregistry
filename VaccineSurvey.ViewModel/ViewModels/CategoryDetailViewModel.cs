﻿using System.Collections.Generic;
using VaccineSurvey.Model;

namespace VaccineSurvey.ViewModel
{
    public class CategoryDetailViewModel
    {
        public CategoryDetailViewModel()
        {
            Category = new Category();
            Questions = new List<Question>();
            Options = new List<QuestionOption>();
        }
        public Category Category { get; set; }
        public List<Question> Questions { get; set; }
        public List<QuestionOption> Options { get; set; }
    }
}
