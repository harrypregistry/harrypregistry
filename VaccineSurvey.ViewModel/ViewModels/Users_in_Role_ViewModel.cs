﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.ViewModel
{
    public class Users_in_Role_ViewModel
    {
        public string Id { get; set; }
        [Required]
        [StringLength(100)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User Roles")]
        public string UserRoles { get; set; }
    }
}
