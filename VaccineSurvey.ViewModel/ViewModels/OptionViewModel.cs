﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.ViewModel
{
    public class OptionViewModel
    {
        public int Id { get; set; }
         
        public int? ParentQuestionId { get; set; }
        public int QuestionId { get; set; }

        [Display(Name = "Option Text")]
        [Required]
        public string OptionText { get; set; }

        [Display(Name = "Option Text Português")]
        public string OptionText_PT { get; set; }

        [Display(Name = "Option Text 漢語")]
        public string OptionText_ZH { get; set; }

        [Display(Name = "Option Text Italiano")]
        public string OptionText_IT { get; set; }

        [Display(Name = "Option Text Français")]
        public string OptionText_FR { get; set; }

        [Display(Name = "Option Text Español")]
        public string OptionText_ES { get; set; }

        [Display(Name = "Option Text Deutsch")]
        public string OptionText_DE { get; set; }

        [Display(Name = "Option Text العربية")]
        public string OptionText_AR { get; set; }

        [Display(Name = "Option Text русский")]
        public string OptionText_RU { get; set; }

        [Display(Name = "Option Text 한국어")]
        public string OptionText_KO { get; set; } 

        [Required]
        public int Sequence { get; set; }

        [Display(Name = "Column Width")]
        public int ColumnWidth { get; set; }
    }
}
