﻿using System;

namespace VaccineSurvey.ViewModel
{
    public class PagingInfo
    {
        public long TotalItems { get; private set; }
        public int TotalPages { get; private set; }
        public int CurrentPage { get; private set; }
        public int PageSize { get; private set; }
        public int StartPage { get; private set; }
        public int EndPage { get; private set; }
        public int FirstItemOnPage { get; private set; }

        public PagingInfo(long totalItems, int page, int pageSize)
        {
            if (pageSize > 0)
            {
                // calculate total, start and end pages
                int totalPages = (int)Math.Ceiling(totalItems / (decimal)pageSize);
                var currentPage = page;
                var startPage = currentPage - 5;
                var endPage = currentPage + 4;
                if (startPage <= 0)
                {
                    endPage -= (startPage - 1);
                    startPage = 1;
                }
                if (endPage > totalPages)
                {
                    endPage = totalPages;
                    if (endPage > 10)
                    {
                        startPage = endPage - 9;
                    }
                }
                TotalItems = totalItems;
                TotalPages = totalPages;
                CurrentPage = currentPage;
                PageSize = pageSize;
                StartPage = startPage;
                EndPage = endPage;
                FirstItemOnPage = (PageSize * (CurrentPage - 1)) + 1;
            }
            else
            {
                TotalItems = totalItems;
                TotalPages = 1;
                CurrentPage = 1;
                PageSize = 0;
                StartPage = 0;
                EndPage = 0;
                FirstItemOnPage = 1;
            }
        }
    }
}
