﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.ViewModel
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email address")]
        public string Email { get; set; }
    }
}
