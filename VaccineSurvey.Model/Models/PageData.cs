﻿using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.Model
{
    public class PageData
    {
        public long Id { get; set; }

        public string PageName { get; set; }

        public string PageURL { get; set; } 


        [Display(Name = "HTML")]
        public string PageHTML { get; set; }

        [Display(Name = "HTML 汉语")]
        public string PageHTML_ZH { get; set; }

        [Display(Name = "HTML Français")]
        public string PageHTML_FR { get; set; } 

        [Display(Name = "HTML Italiano")]
        public string PageHTML_IT { get; set; } 

        [Display(Name = "HTML Português")]
        public string PageHTML_PT { get; set; }

        [Display(Name = "HTML русский")]
        public string PageHTML_RU { get; set; }

        [Display(Name = "HTML Español")]
        public string PageHTML_ES { get; set; }

        [Display(Name = "HTML اردو")]
        public string PageHTML_UR { get; set; }

        [Display(Name = "HTML हिन्दी")]
        public string PageHTML_HI { get; set; }

        [Display(Name = "HTML मराठी")]
        public string PageHTML_MR { get; set; }
    }
}
