﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.Model
{
    public class Category
    {
        public long Id { get; set; }

        [Display(Name = "Category")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Category 汉语")]
        public string Name_ZH { get; set; }

        [Display(Name = "Category Français")]
        public string Name_FR { get; set; }

        [Display(Name = "Category Italiano")]
        public string Name_IT { get; set; }

        [Display(Name = "Category Português")]
        public string Name_PT { get; set; }

        [Display(Name = "Category русский")]
        public string Name_RU { get; set; }

        [Display(Name = "Category Español")]
        public string Name_ES { get; set; }

        [Display(Name = "Category اردو")]
        public string Name_UR { get; set; }

        [Display(Name = "Category हिन्दी")]
        public string Name_HI { get; set; }

        [Display(Name = "Category मराठी")]
        public string Name_MR { get; set; }

        [Required]
        public int Sequence { get; set; }

        [DefaultValue(true)]
        public bool IsVisible { get; set; } = true;

        public virtual List<Question> Questions { get; set; }
    }
}
