﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.Model
{
    public class Question
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }

        [Display(Name = "Question")]
        public string Name { get; set; }

        [Display(Name = "Question 汉语")]
        public string Name_ZH { get; set; }

        [Display(Name = "Question Français")]
        public string Name_FR { get; set; }

        [Display(Name = "Question Italiano")]
        public string Name_IT { get; set; }

        [Display(Name = "Question Português")]
        public string Name_PT { get; set; }

        [Display(Name = "Question русский")]
        public string Name_RU { get; set; }

        [Display(Name = "Question Español")]
        public string Name_ES { get; set; }

        [Display(Name = "Question اردو")]
        public string Name_UR { get; set; }

        [Display(Name = "Question हिन्दी")]
        public string Name_HI { get; set; }

        [Display(Name = "Question मराठी")]
        public string Name_MR { get; set; }

        [Display(Name = "Html Text")]
        public string HtmlText { get; set; } 

        [Display(Name = "Select Category")]
        public long CategoryId { get; set; }

        [Display(Name = "Form Input")]
        [Required]
        public int FormInputId { get; set; }

        [Display(Name = "Required")]
        public bool IsRequired { get; set; }

        [Required]
        public int Sequence { get; set; }

        [DefaultValue(true)]
        public bool IsVisible { get; set; } = true;

        public virtual Category Category { get; set; }

        public virtual FormInput FormInput { get; set; }

        public Question Parent { get; set; }

        public IList<Question> Children { get; set; }

        public virtual List<QuestionOption> QuestionOptions { get; set; }

        public virtual List<QuestionSubmit> QuestionSubmits { get; set; }
    }
}
