﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.Model
{
    public class ReplaceText
    {
        public long Id { get; set; }

        [Display(Name = "Find Text")]
        public string FindText { get; set; }

        [Display(Name = "Replace With")]
        public string ReplaceWith { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}