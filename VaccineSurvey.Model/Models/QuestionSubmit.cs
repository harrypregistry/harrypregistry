﻿using System;

namespace VaccineSurvey.Model
{
    public class QuestionSubmit
    {
        public long Id { get; set; }
        public string CurrentUserId { get; set; }
        public long CategoryId { get; set; }
        public long QuestionId { get; set; }
        public long? QuestionOptionId { get; set; }
        public string Answer { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public virtual Category Category { get; set; }
        public virtual Question Question { get; set; }
        public virtual QuestionOption QuestionOption { get; set; }
    }
}
