﻿using System.Collections.Generic;

namespace VaccineSurvey.Model
{
    public class FormInput
    {
        public int Id { get; set; }
        public string FormInputName { get; set; }
        public List<Question> Questions { get; set; }
    }
}
