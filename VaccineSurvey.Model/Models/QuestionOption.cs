﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace VaccineSurvey.Model
{
    public class QuestionOption
    {
        public long Id { get; set; }
        public long QuestionId { get; set; }

        [Display(Name = "Option")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Option 汉语")]
        public string Name_ZH { get; set; }

        [Display(Name = "Option Français")]
        public string Name_FR { get; set; } 

        [Display(Name = "Option Italiano")]
        public string Name_IT { get; set; } 

        [Display(Name = "Option Português")]
        public string Name_PT { get; set; }

        [Display(Name = "Option русский")]
        public string Name_RU { get; set; }

        [Display(Name = "Option Español")]
        public string Name_ES { get; set; }

        [Display(Name = "Option اردو")]
        public string Name_UR { get; set; }

        [Display(Name = "Option हिन्दी")]
        public string Name_HI { get; set; }

        [Display(Name = "Option मराठी")]
        public string Name_MR { get; set; }

        [Required]
        public int Sequence { get; set; }

        [Display(Name = "Column Width")]
        public int ColumnWidth { get; set; }

        [DefaultValue(true)]
        public bool IsVisible { get; set; } = true;

        public virtual Question Question { get; set; }

        public virtual List<QuestionSubmit> QuestionSubmits { get; set; }
    }
}
