﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace VaccineSurvey.Model
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() { }
        public ApplicationRole(string name) : base(name) { }
        public string Language { get; set; }
    }
}