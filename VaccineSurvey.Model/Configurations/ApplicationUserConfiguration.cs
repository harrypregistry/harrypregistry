﻿using System.Data.Entity.ModelConfiguration;

namespace VaccineSurvey.Model.Configurations
{
    class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            Property(a => a.FirstName)
                .HasMaxLength(250)
                .IsUnicode(false)
                .IsRequired();

            Property(a => a.LastName)
                .HasMaxLength(250)
                .IsUnicode(false)
                .IsRequired();

            Property(a => a.FullName)
                .HasMaxLength(250)
                .IsUnicode(false)
                .IsOptional();
        }
    }
}
