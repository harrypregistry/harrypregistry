﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace VaccineSurvey.Model.Configurations
{
    public class FormInputConfiguration : EntityTypeConfiguration<FormInput>
    {
        public FormInputConfiguration()
        {
            ToTable("FormInputs");

            HasKey(c => c.Id)
                .Property(c => c.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(f => f.FormInputName)
                .HasMaxLength(250)
                .IsUnicode(false)
                .IsRequired();
        }
    }
}
