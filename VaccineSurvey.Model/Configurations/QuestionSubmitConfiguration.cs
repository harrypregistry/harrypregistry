﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace VaccineSurvey.Model.Configurations
{
    public class QuestionSubmitConfiguration : EntityTypeConfiguration<QuestionSubmit>
    {
        public QuestionSubmitConfiguration()
        {
            ToTable("QuestionSubmits");

            HasKey(c => c.Id)
                .Property(c => c.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            HasRequired(q => q.Question)
             .WithMany(c => c.QuestionSubmits)
             .WillCascadeOnDelete(false);

            HasOptional(q => q.QuestionOption)
             .WithMany(qs => qs.QuestionSubmits)
             .WillCascadeOnDelete(false);
        }
    }
}
