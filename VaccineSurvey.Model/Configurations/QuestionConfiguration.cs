﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace VaccineSurvey.Model.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            ToTable("Questions");

            HasKey(q => q.Id)
                .Property(q => q.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(q => q.Name)
                .HasMaxLength(500)
                .IsUnicode(false)
                .IsRequired();

            HasRequired(q => q.Category)
               .WithMany(c => c.Questions)
               .WillCascadeOnDelete(false);

            HasRequired(q => q.FormInput)
               .WithMany(f => f.Questions)
               .WillCascadeOnDelete(false);

            HasOptional(q => q.Parent)
                .WithMany(q => q.Children)
                .HasForeignKey(q => q.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
