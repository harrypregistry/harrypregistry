﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace VaccineSurvey.Model.Configurations
{
    public class QuestionOptionConfiguration : EntityTypeConfiguration<QuestionOption>
    {
        public QuestionOptionConfiguration()
        {
            ToTable("QuestionOptions");

            HasKey(qo => qo.Id)
                .Property(qo => qo.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(qo => qo.Name)
                .HasMaxLength(500)
                .IsUnicode(false)
                .IsRequired();

            HasRequired(qo => qo.Question)
               .WithMany(q => q.QuestionOptions)
               .WillCascadeOnDelete(false);
        }
    }
}
