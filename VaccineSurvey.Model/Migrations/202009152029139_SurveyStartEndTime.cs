namespace VaccineSurvey.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SurveyStartEndTime : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuestionSubmits", "StartTime", c => c.DateTime());
            AddColumn("dbo.QuestionSubmits", "EndTime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuestionSubmits", "EndTime");
            DropColumn("dbo.QuestionSubmits", "StartTime");
        }
    }
}
