namespace VaccineSurvey.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoleLanguage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetRoles", "Language", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetRoles", "Language");
        }
    }
}
