namespace VaccineSurvey.Model.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250, unicode: false),
                        Name_ZH = c.String(),
                        Name_FR = c.String(),
                        Name_IT = c.String(),
                        Name_PT = c.String(),
                        Name_RU = c.String(),
                        Name_ES = c.String(),
                        Name_UR = c.String(),
                        Name_HI = c.String(),
                        Name_MR = c.String(),
                        Sequence = c.Int(nullable: false),
                        IsVisible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ParentId = c.Long(),
                        Name = c.String(nullable: false, maxLength: 500, unicode: false),
                        Name_ZH = c.String(),
                        Name_FR = c.String(),
                        Name_IT = c.String(),
                        Name_PT = c.String(),
                        Name_RU = c.String(),
                        Name_ES = c.String(),
                        Name_UR = c.String(),
                        Name_HI = c.String(),
                        Name_MR = c.String(),
                        HtmlText = c.String(),
                        CategoryId = c.Long(nullable: false),
                        FormInputId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        Sequence = c.Int(nullable: false),
                        IsVisible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId)
                .ForeignKey("dbo.FormInputs", t => t.FormInputId)
                .ForeignKey("dbo.Questions", t => t.ParentId)
                .Index(t => t.ParentId)
                .Index(t => t.CategoryId)
                .Index(t => t.FormInputId);
            
            CreateTable(
                "dbo.FormInputs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FormInputName = c.String(nullable: false, maxLength: 250, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionOptions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuestionId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 500, unicode: false),
                        Name_ZH = c.String(),
                        Name_FR = c.String(),
                        Name_IT = c.String(),
                        Name_PT = c.String(),
                        Name_RU = c.String(),
                        Name_ES = c.String(),
                        Name_UR = c.String(),
                        Name_HI = c.String(),
                        Name_MR = c.String(),
                        Sequence = c.Int(nullable: false),
                        ColumnWidth = c.Int(nullable: false),
                        IsVisible = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Questions", t => t.QuestionId)
                .Index(t => t.QuestionId);
            
            CreateTable(
                "dbo.QuestionSubmits",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CurrentUserId = c.String(),
                        CategoryId = c.Long(nullable: false),
                        QuestionId = c.Long(nullable: false),
                        QuestionOptionId = c.Long(),
                        Answer = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Questions", t => t.QuestionId)
                .ForeignKey("dbo.QuestionOptions", t => t.QuestionOptionId)
                .Index(t => t.CategoryId)
                .Index(t => t.QuestionId)
                .Index(t => t.QuestionOptionId);
            
            CreateTable(
                "dbo.PageDatas",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PageName = c.String(),
                        PageURL = c.String(),
                        PageHTML = c.String(),
                        PageHTML_ZH = c.String(),
                        PageHTML_FR = c.String(),
                        PageHTML_IT = c.String(),
                        PageHTML_PT = c.String(),
                        PageHTML_RU = c.String(),
                        PageHTML_ES = c.String(),
                        PageHTML_UR = c.String(),
                        PageHTML_HI = c.String(),
                        PageHTML_MR = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReplaceTexts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FindText = c.String(),
                        ReplaceWith = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(nullable: false, maxLength: 250, unicode: false),
                        LastName = c.String(nullable: false, maxLength: 250, unicode: false),
                        FullName = c.String(maxLength: 250, unicode: false),
                        CreatedDate = c.DateTime(),
                        UpdatedDate = c.DateTime(),
                        UserDate = c.String(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.QuestionSubmits", "QuestionOptionId", "dbo.QuestionOptions");
            DropForeignKey("dbo.QuestionSubmits", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.QuestionSubmits", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.QuestionOptions", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "ParentId", "dbo.Questions");
            DropForeignKey("dbo.Questions", "FormInputId", "dbo.FormInputs");
            DropForeignKey("dbo.Questions", "CategoryId", "dbo.Categories");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.QuestionSubmits", new[] { "QuestionOptionId" });
            DropIndex("dbo.QuestionSubmits", new[] { "QuestionId" });
            DropIndex("dbo.QuestionSubmits", new[] { "CategoryId" });
            DropIndex("dbo.QuestionOptions", new[] { "QuestionId" });
            DropIndex("dbo.Questions", new[] { "FormInputId" });
            DropIndex("dbo.Questions", new[] { "CategoryId" });
            DropIndex("dbo.Questions", new[] { "ParentId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.ReplaceTexts");
            DropTable("dbo.PageDatas");
            DropTable("dbo.QuestionSubmits");
            DropTable("dbo.QuestionOptions");
            DropTable("dbo.FormInputs");
            DropTable("dbo.Questions");
            DropTable("dbo.Categories");
        }
    }
}
