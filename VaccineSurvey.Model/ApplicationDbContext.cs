﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using VaccineSurvey.Model.Configurations;

namespace VaccineSurvey.Model
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("VaccineSurveyConnection", throwIfV1Schema: false) { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionOption> QuestionOptions { get; set; }
        public DbSet<FormInput> FormInputs { get; set; }
        public DbSet<QuestionSubmit> QuestionSubmits { get; set; }
        public DbSet<PageData> PageData { get; set; }
        public DbSet<ReplaceText> ReplaceText { get; set; }
        public static ApplicationDbContext Create() => new ApplicationDbContext();
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ApplicationUserConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new QuestionOptionConfiguration());
            modelBuilder.Configurations.Add(new FormInputConfiguration());
            modelBuilder.Configurations.Add(new QuestionSubmitConfiguration());
            base.OnModelCreating(modelBuilder);
        }

        public System.Data.Entity.DbSet<VaccineSurvey.Model.ApplicationRole> IdentityRoles { get; set; }
    }
}