﻿using System;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web
{
    public class UserRoleDetails
    {
        public static string GetLanguage()
        {
            try
            {
                var _context = HttpContext.Current;
                if (_context.User.Identity.IsAuthenticated)
                {
                    var userManager = _context.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

                    var userRoles = userManager.GetRoles(_context.User.Identity.GetUserId());
                    var role = userRoles[0];
                    ApplicationRole userRole = new ApplicationDbContext().IdentityRoles.FirstOrDefault(x => x.Name.ToLower() == role.ToLower());
                    if (userRole != null) return userRole.Language.ToLower();
                }
            }
            catch { }
            return "";
        }

        public static string GetRole()
        {
            try
            {
                var _context = HttpContext.Current;
                if (_context.User.Identity.IsAuthenticated)
                {
                    var userManager = _context.Request.GetOwinContext().GetUserManager<ApplicationUserManager>(); 
                    var userRoles = userManager.GetRoles(_context.User.Identity.GetUserId());
                    return userRoles[0];
                }
            }
            catch { }
            return "";
        }

        public static string GetUserName()
        {
            try
            {
                var _context = HttpContext.Current;
                if (_context.User.Identity.IsAuthenticated)
                {
                    var userManager = _context.Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                    var user = userManager.FindById(_context.User.Identity.GetUserId());
                    var userRoles = userManager.GetRoles(_context.User.Identity.GetUserId());
                    if (user != null) return user.FirstName + " " + user.LastName + " (" + userRoles[0] + ")";
                }
            }
            catch { }
            return "";
        }

        public static string GetPropValue(object src, string propertyName, string Language)
        {
            try
            {
                if (Language != "admin" && Language != "" && Language != "en") propertyName += "_" + Language.ToUpper();

                var r = src.GetType().GetProperty(propertyName).GetValue(src, null);
                if (r != null) { return r.ToString().Replace("'", @"\'"); }
                return "";
            }
            catch { return ""; }
        }
    }
}