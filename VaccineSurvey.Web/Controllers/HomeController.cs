﻿using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Common.Helpers;
using VaccineSurvey.Data.Repositories;

namespace VaccineSurvey.Web.Controllers
{
    [HandleError]
    [RequiresSSL]
    public class HomeController : Controller
    {
        private readonly IDashboardRepository dashboardRepository;
        public HomeController(IDashboardRepository dashboardRepository) => this.dashboardRepository = dashboardRepository;

        public async Task<ActionResult> Index()
        {
            ViewBag.PageData = await dashboardRepository.GetPageDataByName("Home");
            return View();
        }

        public async Task<ActionResult> Information()
        {
            ViewBag.PageData = await dashboardRepository.GetPageDataByName("Information");
            return View();
        }
    }
}