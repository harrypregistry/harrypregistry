﻿using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class OptionsController : Controller
    {
        private readonly IOptionRepository optionRepository;

        public OptionsController(IOptionRepository optionRepository) => this.optionRepository = optionRepository;

        public async Task<ActionResult> Edit(int? id, int? parentQuestionId)
        {
            if (id == null || parentQuestionId == null) { return RedirectToAction("Error"); }
            var model = await optionRepository.FindByIdAsync(id);
            if (model == null) { return RedirectToAction("Error"); }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(QuestionOption viewModel)
        { 
            if (!ModelState.IsValid) { return View(viewModel); }

            var Succeeded = await optionRepository.UpdateAsync(viewModel, UserRoleDetails.GetLanguage());
            if (Succeeded) { return RedirectToAction("Details", "Questions", new { id = viewModel.QuestionId }); }
            return RedirectToAction("Edit", new { id = viewModel.Id });
        }

        public async Task<ActionResult> Delete(int? id, int? parentQuestionId)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (id == null || parentQuestionId == null) { return RedirectToAction("Error"); }
                var model = await optionRepository.FindByIdAsync(id);
                if (model == null) { return RedirectToAction("Error"); }
                return View(model);
            }
            return RedirectToAction("Index", "Questions");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(QuestionOption viewModel)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                var Succeeded = await optionRepository.DeleteAsync(viewModel.Id);
                if (!Succeeded) { return RedirectToAction("Error"); }
                return RedirectToAction("Details", "Questions", new { id = viewModel.QuestionId });
            }
            return RedirectToAction("Index", "Questions");
        }
    }
}