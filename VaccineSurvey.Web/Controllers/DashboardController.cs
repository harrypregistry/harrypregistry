﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using OfficeOpenXml;
using VaccineSurvey.Data.Repositories;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DashboardController : Controller
    {
        private readonly IDashboardRepository dashboardRepository;

        public DashboardController(IDashboardRepository dashboardRepository) => this.dashboardRepository = dashboardRepository;

        #region Dashboard

        public async Task<ActionResult> Index() => View(await dashboardRepository.GetResultsUserList());

        public async Task<ActionResult> Result(string Id)
        {
            if (Id != null && Id.Length > 0) { return View(await dashboardRepository.GetResults(Id)); }
            else { return RedirectToAction("Index", "Dashboard"); }
        }

        #endregion

        #region New Dashboard

        public ActionResult New() => View();

        public async Task<PartialViewResult> GetList(string FromDate, string Todate) => PartialView("_List", await dashboardRepository.GetResultsUserList(DateTime.ParseExact(FromDate, "dd/MM/yyyy", null), DateTime.ParseExact(Todate, "dd/MM/yyyy", null)));

        public async Task<ActionResult> NewResult(string FromDate, string Todate) => View(await dashboardRepository.GetExcelAsync(DateTime.ParseExact(FromDate, "dd/MM/yyyy", null), DateTime.ParseExact(Todate, "dd/MM/yyyy", null)));

        public async Task<ActionResult> Download(string FromDate, string Todate)
        {
            var obj = await dashboardRepository.GetExcelAsync(DateTime.ParseExact(FromDate, "dd/MM/yyyy", null), DateTime.ParseExact(Todate, "dd/MM/yyyy", null));

            string fileHeading = "Vaccine Survey";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (ExcelPackage Ep = new ExcelPackage())
            {
                ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add(fileHeading);
                Sheet.Cells["A1"].Value = "User Id";
                Sheet.Cells["B1"].Value = "Date";

                int row = 2;
                bool columnAdded = false;
                foreach (var user in obj.Users)
                {
                    Sheet.Cells[string.Format("A{0}", row)].Value = user.CurrentUserId;
                    Sheet.Cells[string.Format("B{0}", row)].Value = user.CreatedDate.ToString("dd MMM yyyy hh:mm tt");

                    int colNo = 3;
                    foreach (var item in obj.ExcelQuestions)
                    {
                        if (!columnAdded)
                        {
                            Sheet.Cells[1, colNo].Value = item.ColumnName;
                            if (item.ColumnName.Contains("[Other")) { Sheet.Cells[1, colNo + 1].Value = item.ColumnName + " Please Specify"; }

                            if (item.Id == 11)
                            {
                                for (int j = 1; j <= 10; j++)
                                {
                                    Sheet.Cells[1, colNo + j].Value = "Child " + j;
                                }
                            }
                            if (item.Id == 16 || item.Id == 33 || item.Id == 35 || item.Id == 37)
                            {
                                Sheet.Cells[1, colNo + 1].Value = item.ColumnName + " Please Specify";
                            }
                        }

                        if (item.OptionId == 0)
                        {
                            var answers = obj.QuestionSubmit.Where(x => x.CurrentUserId == user.CurrentUserId && x.QuestionId == item.Id).FirstOrDefault();
                            if (answers != null) { Sheet.Cells[row, colNo].Value = answers.Answer.Split(new[] { "->" }, StringSplitOptions.None)[0]; }

                            if (item.Id == 11)
                            {
                                var children = obj.QuestionSubmit.Where(x => x.CurrentUserId == user.CurrentUserId && x.QuestionId == 12).OrderBy(x => x.Id).ToList();
                                for (int j = 0; j < 10; j++)
                                {
                                    colNo++;
                                    string childAge = "";
                                    try { childAge = children[j].Answer; } catch { }
                                    Sheet.Cells[row, colNo].Value = childAge;
                                }
                            }

                            if (item.Id == 16 || item.Id == 33 || item.Id == 35 || item.Id == 37)
                            {
                                colNo++;
                                var ans = "";
                                if (answers != null)
                                {
                                    if (!string.IsNullOrEmpty(answers.Answer) && !string.IsNullOrWhiteSpace(answers.Answer))
                                    {
                                        try
                                        {
                                            ans = answers.Answer.Split(new[] { "->" }, StringSplitOptions.None)[1]; ;
                                        }
                                        catch { }
                                    }
                                }
                                Sheet.Cells[row, colNo].Value = ans;
                            }
                        }
                        else
                        {
                            var answer = obj.QuestionSubmit.Where(x => x.CurrentUserId == user.CurrentUserId && x.QuestionId == item.Id && x.QuestionOptionId == item.OptionId).FirstOrDefault();
                            if (answer != null)
                            {
                                Sheet.Cells[row, colNo].Value = "1";
                                if (item.ColumnName.Contains("[Other")) { Sheet.Cells[row, colNo + 1].Value = answer.Answer; }
                            }
                            if (item.ColumnName.Contains("[Other")) { colNo++; }
                        }
                        colNo++;
                    }
                    columnAdded = true;
                    row++;
                }

                //Sheet.Cells[Sheet.Dimension.Address].AutoFitColumns();
                string fileName = fileHeading + "_" + DateTime.Now.ToString("yyyyMMddhhmmsstt") + ".xlsx";
                return File(Ep.GetAsByteArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", fileName);
            }
        }

        #endregion

        #region Country Base Result 

        public async Task<ActionResult> Country() => View(await dashboardRepository.GetCountryCount());

        public async Task<ActionResult> CountryFeedback() => View(await dashboardRepository.GetCountryFeedback());





        [HttpPost]
        public async Task<ActionResult> ExportData()
        {

            var result = await dashboardRepository.GetCountryFeedback();

            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                ExcelPackage Ep = new ExcelPackage();
                ExcelWorksheet Sheet = Ep.Workbook.Worksheets.Add("Report");
                Sheet.Cells["A1"].Value = "Country";
                Sheet.Cells["B1"].Value = "CurrentUserId";
                Sheet.Cells["C1"].Value = "Feedback";
                Sheet.Cells["D1"].Value = "FeedbackDate";

                int row = 2;
                foreach (var item in result)
                {

                    Sheet.Cells[string.Format("A{0}", row)].Value = item.Country;
                    Sheet.Cells[string.Format("B{0}", row)].Value = item.CurrentUserId;
                    Sheet.Cells[string.Format("C{0}", row)].Value = item.Feedback;
                    Sheet.Cells[string.Format("D{0}", row)].Value = item.FeedbackDate;

                    row++;
                }


                Sheet.Cells["A:AZ"].AutoFitColumns();
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment: filename=" + "Report.xlsx");
                Response.BinaryWrite(Ep.GetAsByteArray());
                Response.End();



                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        #endregion
    }
}