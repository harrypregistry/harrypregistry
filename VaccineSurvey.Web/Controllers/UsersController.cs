﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;
using VaccineSurvey.ViewModel;
using VaccineSurvey.Web.Models;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class UsersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private readonly IAuthRepository authRepository;

        public UsersController(IAuthRepository _AuthRepository) => authRepository = _AuthRepository;

        public UsersController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.User = User.Identity.GetUserId();
            var usersWithRoles = (from user in db.Users
                                  select new
                                  {
                                      Id = user.Id,
                                      FirstName = user.FirstName,
                                      LastName = user.LastName,
                                      Email = user.Email,
                                      RoleNames = (from userRole in user.Roles
                                                   join role in db.Roles on userRole.RoleId
                                                   equals role.Id
                                                   select role.Name).FirstOrDefault()
                                  }).ToList().Select(p => new Users_in_Role_ViewModel()
                                  {
                                      Id = p.Id,
                                      FirstName = p.FirstName,
                                      LastName = p.LastName,
                                      Email = p.Email,
                                      UserRoles = p.RoleNames
                                  });

            return View(usersWithRoles);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            RegisterViewModel viewModel = new RegisterViewModel();
            ViewBag.Name = new SelectList(db.Roles.OrderBy(x => x.Name).ToList(), "Name", "Name");
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel viewModel)
        {
            string errors = "Sorry an error occured. Try again.";
            if (ModelState.IsValid)
            {
                var appUser = new ApplicationUser
                {
                    FirstName = viewModel.FirstName,
                    LastName = viewModel.LastName,
                    Email = viewModel.Email,
                    UserName = viewModel.Email,
                    CreatedDate = DateTime.Now
                };

                var result = await UserManager.CreateAsync(appUser, viewModel.Password);
                if (result.Succeeded)
                {
                    try
                    {
                        await UserManager.AddToRoleAsync(appUser.Id, viewModel.UserRoles);
                    }
                    catch (Exception ex)
                    {
                        errors = ex.Message;
                    }
                    TempData["Success"] = "User saved successfully";
                    return RedirectToAction("Create");
                }
            }
            else
            {
                errors = "";
                foreach (ModelState modelState in ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errors += error.ErrorMessage + "<br/>";
                    }
                }
            }
            ViewBag.Name = new SelectList(db.Roles.OrderBy(x => x.Name).ToList(), "Id", "Name");
            TempData["Error"] = errors;
            return View(viewModel);
        }

        public async Task<ActionResult> EditProfile()
        {

            var appUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (appUser == null) { return RedirectToAction("Error"); }

            var viewModel = new EditProfileViewModel
            {
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                Email = appUser.Email
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditProfile(EditProfileViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }
            var appUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (appUser == null) { return RedirectToAction("Error"); }
            appUser.FirstName = viewModel.FirstName;
            appUser.LastName = viewModel.LastName;

            var result = await UserManager.UpdateAsync(appUser);
            if (result.Succeeded)
            {
                TempData["Success"] = "Profile Successfully Updated.";
            }
            else
            {
                TempData["Error"] = "Sorry...! Please try again.";
            }
            return RedirectToAction("EditProfile");
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string Id)
        {
            var appUser = await UserManager.FindByIdAsync(Id);
            if (appUser == null) { return RedirectToAction("Error"); }

            var usersWithRoles = (from user in db.Users
                                  select new
                                  {
                                      Id = user.Id,
                                      Username = user.UserName,
                                      FirstName = user.FirstName,
                                      LastName = user.LastName,
                                      Email = user.Email,
                                      RoleNames = (from userRole in user.Roles
                                                   join role in db.Roles on userRole.RoleId
                                                   equals role.Id
                                                   select role.Name).FirstOrDefault()
                                  }).Where(x => x.Id == appUser.Id).Select(p => new Users_in_Role_ViewModel()
                                  {
                                      Id = p.Id,
                                      FirstName = p.FirstName,
                                      LastName = p.LastName,
                                      Email = p.Email,
                                      UserRoles = p.RoleNames
                                  });

            ViewBag.Name = new SelectList(db.Roles.OrderBy(x => x.Name).ToList(), "Name", "Name");
            return View(usersWithRoles.FirstOrDefault());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Users_in_Role_ViewModel viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }
            var appUser = await UserManager.FindByIdAsync(viewModel.Id);
            if (appUser == null) { return RedirectToAction("Error"); }
            appUser.FirstName = viewModel.FirstName;
            appUser.LastName = viewModel.LastName;

            var result = await UserManager.UpdateAsync(appUser);
            if (result.Succeeded)
            {
                var oldRoleId = appUser.Roles.SingleOrDefault();
                if (oldRoleId != null)
                {
                    var oldRoleName = db.Roles.SingleOrDefault(r => r.Id == oldRoleId.RoleId).Name;

                    if (oldRoleName != viewModel.UserRoles)
                    {
                        await UserManager.RemoveFromRoleAsync(viewModel.Id, oldRoleName);
                        await UserManager.AddToRoleAsync(viewModel.Id, viewModel.UserRoles);
                    }
                }
                else
                    await UserManager.AddToRoleAsync(viewModel.Id, viewModel.UserRoles);
                TempData["Success"] = "User updated successfully.";
            }
            else
            {
                TempData["Error"] = "Sorry...! Please try again.";
            }
            ViewBag.Name = new SelectList(db.Roles.OrderBy(x => x.Name).ToList(), "Name", "Name");
            return RedirectToAction("Edit", viewModel);
        }

        public ActionResult ChangePassword() => View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }

                if (result.Succeeded)
                {
                    TempData["Success"] = "Password Successfully Updated.";
                    return RedirectToAction("EditProfile");
                }
            }
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string Id)
        {
            if (Id == User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            var appUser = await UserManager.FindByIdAsync(Id);
            if (appUser == null) { return RedirectToAction("Error"); }

            var viewModel = new EditProfileViewModel
            {
                Id = appUser.Id,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                Email = appUser.Email
            };
            return View(viewModel);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string Id)
        {
            if (Id == User.Identity.GetUserId())
            {
                return RedirectToAction("Index");
            }
            var appUser = await UserManager.FindByIdAsync(Id);
            var result = await UserManager.DeleteAsync(appUser);
            if (result.Succeeded)
            {
                TempData["Success"] = "User deleted successfully.";
            }
            else
            {
                TempData["Error"] = "Sorry...! Please try again.";
            }
            return RedirectToAction("Index");
        }
    }
}