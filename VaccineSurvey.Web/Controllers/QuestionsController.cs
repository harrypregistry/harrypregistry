﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class QuestionsController : Controller
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();
        private readonly IQuestionRepository questionRepository;
        private readonly IOptionRepository OptionRepository;
        private readonly IFormInputRepository formInputRepository;
        private readonly ICategoryRepository categoryRepository;

        public QuestionsController(IQuestionRepository questionRepository, IFormInputRepository formInputRepository, ICategoryRepository categoryRepository, IOptionRepository OptionRepository)
        {
            this.OptionRepository = OptionRepository;
            this.questionRepository = questionRepository;
            this.formInputRepository = formInputRepository;
            this.categoryRepository = categoryRepository;
        }

        public async Task<ActionResult> Index(int? id)
        {
            IEnumerable<Question> questions = null;
            if (id != null && id > 0) { questions = await categoryRepository.GetQuestionsAsync(id); }
            if (questions != null && questions.Count() > 0) { }
            else { questions = await questionRepository.GetAllAsync(); }
            ViewBag.FormInputs = await formInputRepository.GetAllAsync();
            ViewBag.Options = await OptionRepository.GetAllAsync();
            ViewBag.ChildQuestions= await questionRepository.GetChildQuestion();
            return View(questions);
        }

        public async Task<ActionResult> Create(int? id)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                int categoryId = 0;
                if (id != null) { categoryId = (int)id; }
                ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", categoryId);
                ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName");
                var model = new Question
                {
                    IsRequired = true,
                    Sequence = 0
                };
                return View(model);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Question viewModel)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", viewModel.CategoryId);
                ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", viewModel.FormInputId);
                if (!ModelState.IsValid) { return View(viewModel); }
                var questionId = await questionRepository.CreateAsync(viewModel);
                if (questionId == 0)
                {
                    TempData["Error"] = "Try again input data error";
                    return RedirectToAction("Create", viewModel);
                }
                TempData["Success"] = "Question Saved successfully.";
                return RedirectToAction("Create");
            }
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int? id, int? parentQuestionId)
        {
            if (id == null) { return RedirectToAction("Error"); }
            var model = await questionRepository.FindByIdAsync(id);
            if (model == null) { return RedirectToAction("Error"); }
            ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", model.FormInputId);
            ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", model.CategoryId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Question viewModel)
        {
            ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", viewModel.FormInputId);
            ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", viewModel.CategoryId);

            if (!ModelState.IsValid) { return View(viewModel); }
            var Succeeded = await questionRepository.UpdateAsync(viewModel, UserRoleDetails.GetLanguage());
            if (!Succeeded) { return RedirectToAction("Error"); }
            else { TempData["Success"] = "Question Updated successfully."; }
            return RedirectToAction("Edit", new { id = viewModel.Id });
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null) { return RedirectToAction("Index"); }
            var viewModel = await questionRepository.DetailsAsync(id);
            if (viewModel == null) { return RedirectToAction("Index"); }

            viewModel.ChildQuestions = await _context.Questions.Include(q => q.Children).Where(q => q.ParentId == id).ToListAsync();
            ViewBag.DataModel = viewModel;
            ViewBag.FormInputs = await formInputRepository.GetAllAsync();
            return View(new QuestionOption() { QuestionId = (int)id });
        }

        public async Task<ActionResult> Delete(int? id, int? parentQuestionId)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (id == null) { return RedirectToAction("Index"); }
                var viewModel = await questionRepository.FindByIdAsync(id);
                if (viewModel == null) { return RedirectToAction("Index"); }
                ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", viewModel.FormInputId);
                ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", viewModel.CategoryId);
                return View(viewModel);
            }
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? Id, int? ParentQuestionId)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (Id == null) { return RedirectToAction("Index"); }
                var Succeeded = await questionRepository.DeleteAsync(Id);
                if (!Succeeded) { return RedirectToAction("Index"); }
                if (ParentQuestionId != null) { return RedirectToAction("Details", new { id = ParentQuestionId }); }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddOption(QuestionOption viewModel)
        {
            if (!ModelState.IsValid) { return View(); }
            _context.QuestionOptions.Add(viewModel);
            _context.SaveChanges();
            return RedirectToAction("Details", new { id = viewModel.QuestionId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteOption(int? id)
        {
            if (id == null) { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
            var option = _context.QuestionOptions.Find(id);
            if (option == null) { return Json(new { success = false }, JsonRequestBehavior.AllowGet); }
            _context.QuestionOptions.Remove(option);
            _context.SaveChanges();
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> AddNestedQuestion(int? id)
        {
            if (id == null) { return RedirectToAction("Index"); }
            var question = await _context.Questions.Where(q => q.Id == id).FirstOrDefaultAsync();
            if (question == null) { return RedirectToAction("Index"); }
            var viewModel = new Question
            {
                Id = question.Id,
                CategoryId = question.CategoryId,
                ParentId = question.Id
            };
            ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", question.CategoryId);
            viewModel.Children = _context.Questions.Where(q => q.ParentId == id).ToList();

            var FormInputs = await formInputRepository.GetAllAsync();
            ViewBag.SelectFormInputId = new SelectList(FormInputs, "Id", "FormInputName");

            ViewBag.Options = await OptionRepository.GetAllAsync();
            ViewBag.FormInputs = FormInputs;
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddNestedQuestion(Question viewModel)
        {
            if (!ModelState.IsValid) { return RedirectToAction("Index"); }
            var question = await _context.Questions.Where(q => q.Id == viewModel.Id).FirstOrDefaultAsync();
            if (question == null) { return RedirectToAction("Index"); }
            _context.Questions.Add(viewModel);
            _context.SaveChanges();
            TempData["Success"] = "Successfully added";
            return RedirectToAction("AddNestedQuestion", new { id = viewModel.Id });
        }
    }
}