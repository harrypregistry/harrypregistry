﻿using System;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleManagementController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: RoleManagement
        public async Task<ActionResult> Index() => View(await db.IdentityRoles.ToListAsync());

        // GET: RoleManagement/Create
        public ActionResult Create() => View(new ApplicationRole() { Id = Guid.NewGuid().ToString() });

        // POST: RoleManagement/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Language")] ApplicationRole applicationRole)
        {
            if (ModelState.IsValid)
            {
                ApplicationRole AdminRole = await db.IdentityRoles.FirstOrDefaultAsync(x => x.Name.ToLower() == applicationRole.Name.ToLower());
                if (AdminRole == null)
                {
                    db.IdentityRoles.Add(applicationRole);
                    await db.SaveChangesAsync();
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Error"] = "This role already exists.";
                }
            }
            return View(applicationRole);
        }

        // GET: RoleManagement/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ApplicationRole applicationRole = await db.IdentityRoles.FindAsync(id);
            if (applicationRole == null) { return HttpNotFound(); }
            if (applicationRole.Name.ToLower() == "admin") { return RedirectToAction("Index"); }

            return View(applicationRole);
        }

        // POST: RoleManagement/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Language")] ApplicationRole applicationRole)
        {
            if (ModelState.IsValid)
            { 
                db.Entry(applicationRole).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(applicationRole);
        }

        // GET: RoleManagement/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            ApplicationRole applicationRole = await db.IdentityRoles.FindAsync(id);
            if (applicationRole == null) { return HttpNotFound(); }
            if (applicationRole.Name.ToLower() == "admin") { return RedirectToAction("Index"); }
            return View(applicationRole);
        }

        // POST: RoleManagement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            ApplicationRole applicationRole = await db.IdentityRoles.FindAsync(id);
            if (applicationRole.Name.ToLower() != "admin")
            {
                db.IdentityRoles.Remove(applicationRole);
                await db.SaveChangesAsync();
            }
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
