﻿using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class LoopQuestionsController : Controller
    {
        private readonly IQuestionRepository questionRepository;
        private readonly IFormInputRepository formInputRepository;
        private readonly ICategoryRepository categoryRepository;
        public LoopQuestionsController(IQuestionRepository questionRepository, IFormInputRepository formInputRepository, ICategoryRepository categoryRepository)
        {
            this.questionRepository = questionRepository;
            this.formInputRepository = formInputRepository;
            this.categoryRepository = categoryRepository;
        }

        public async Task<ActionResult> Edit(int? id, int? parentQuestionId)
        {
            if (id == null) { return RedirectToAction("Error"); }
            var model = await questionRepository.FindByIdAsync(id);
            if (model == null) { return RedirectToAction("Error"); }
            ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", model.FormInputId);
            ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", model.CategoryId);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Question viewModel)
        { 
            if (!ModelState.IsValid) { return View(viewModel); }
            var Succeeded = await questionRepository.UpdateAsync(viewModel, UserRoleDetails.GetLanguage());
            if (!Succeeded) { return RedirectToAction("Error"); }
            else TempData["Success"] = "Successfully Updated.";

            return RedirectToAction("Edit", new { id = viewModel.Id });
        }

        public async Task<ActionResult> Delete(int? id, int? parentQuestionId)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (id == null) { return RedirectToAction("Index"); }
                var viewModel = await questionRepository.FindByIdAsync(id);
                if (viewModel == null) { return RedirectToAction("Index"); }
                ViewBag.SelectFormInputId = new SelectList(await formInputRepository.GetAllAsync(), "Id", "FormInputName", viewModel.FormInputId);
                ViewBag.SelectCategoryId = new SelectList(await categoryRepository.GetAllAsync(), "Id", "Name", viewModel.CategoryId);
                return View(viewModel);
            }
            return RedirectToAction("Index", "Questions");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? Id, int? ParentQuestionId)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (Id == null) { return RedirectToAction("Index"); }
                var Succeeded = await questionRepository.DeleteAsync(Id);
                if (!Succeeded) { return RedirectToAction("Index"); }
                if (ParentQuestionId != null) { return RedirectToAction("AddNestedQuestion", "Questions", new { id = ParentQuestionId }); }
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index", "Questions");
        }
    }
}