﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class ReplaceTextController : Controller
    {
        private readonly IReplaceTextRepository replaceTextRepository;
        private readonly IUserRepository userRepository;

        public ReplaceTextController(IReplaceTextRepository _replaceTextRepository,
            IUserRepository userRepository) 
        {
            replaceTextRepository = _replaceTextRepository;
            this.userRepository = userRepository;
        } 

        public ActionResult Questions()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Questions(ReplaceText viewModel)
        {
            if (ModelState.IsValid)
            {
                var roleName = await userRepository.GetRolesAsync(User.Identity.GetUserId());
                var Succeeded = await replaceTextRepository.QuestionsAsync(viewModel, roleName);
                if (Succeeded)
                {
                    TempData["Success"] = "Text Replaced Successfully.";
                    return RedirectToAction("Questions");
                }
            }
            TempData["Error"] = "Sorry...! An error occurred.";
            ViewBag.Data = await replaceTextRepository.GetAllAsync();
            return View(viewModel);
        }

        public ActionResult Options()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Options(ReplaceText viewModel)
        {
            if (ModelState.IsValid)
            {
                var roleName = await userRepository.GetRolesAsync(User.Identity.GetUserId());
                var Succeeded = await replaceTextRepository.OptionsAsync(viewModel, roleName);
                if (Succeeded)
                {
                    TempData["Success"] = "Text Replaced Successfully.";
                    return RedirectToAction("Options");
                }
            }
            TempData["Error"] = "Sorry...! An error occurred.";
            ViewBag.Data = await replaceTextRepository.GetAllAsync();
            return View(viewModel);
        }
    }
}