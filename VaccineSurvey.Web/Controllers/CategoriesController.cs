﻿using System.Threading.Tasks;
using System.Web.Mvc;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Model;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly ICategoryRepository categoryRepository;
        private readonly IQuestionRepository questionRepository;

        public CategoriesController(IQuestionRepository questionRepository, ICategoryRepository categoryRepository)
        {
            this.questionRepository = questionRepository;
            this.categoryRepository = categoryRepository;
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.Questions = await questionRepository.GetAllAsync();
            return View(await categoryRepository.GetAllAsync());
        }

        public ActionResult Create()
        {
            if (UserRoleDetails.GetLanguage() == "admin") return View();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Category viewModel)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (ModelState.IsValid)
                {
                    var Succeeded = await categoryRepository.CreateAsync(viewModel);
                    if (Succeeded)
                    {
                        TempData["Success"] = "Category saved successfully.";
                        return RedirectToAction("Create");
                    }
                }
                return View(viewModel);
            }
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null) { return RedirectToAction("Index"); }
            var category = await categoryRepository.FindByIdAsync(id);
            if (category == null) { return RedirectToAction("Index"); }
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Category viewModel)
        {
            if (!ModelState.IsValid) { return View(viewModel); }

            var Succeeded = await categoryRepository.UpdateAsync(viewModel, UserRoleDetails.GetLanguage());
            if (Succeeded)
            {
                TempData["Success"] = "Category Updated successfully.";
                return RedirectToAction("Edit", new { id = viewModel.Id });
            }

            return RedirectToAction("Edit", new { id = viewModel.Id });
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (id == null) { return RedirectToAction("Index"); }
                var category = await categoryRepository.FindByIdAsync(id);
                if (category == null) { return RedirectToAction("Index"); }
                return View(category);
            }
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            if (UserRoleDetails.GetLanguage() == "admin")
            {
                if (id == null)
                {
                    TempData["Error"] = "Try again input data error";
                    return RedirectToAction("Index");
                }
                var Succeeded = await categoryRepository.DeleteAsync(id);
                if (Succeeded)
                {
                    TempData["Success"] = "Category deleted successfully";
                    return RedirectToAction("Index");
                }
                TempData["Error"] = "Try again input data error";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
