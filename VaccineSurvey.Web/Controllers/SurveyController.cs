﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using VaccineSurvey.Data.Repositories;
using VaccineSurvey.Dto;
using VaccineSurvey.ViewModel;

namespace VaccineSurvey.Web.Controllers
{
    public class SurveyController : Controller
    {
        private readonly IFormRepository formRepository;
        private readonly IDashboardRepository dashboardRepository;
        public SurveyController(IDashboardRepository dashboardRepository, IFormRepository formRepository)
        {
            this.dashboardRepository = dashboardRepository;
            this.formRepository = formRepository;
        }

        public async Task<ActionResult> Index()
        {
            ViewBag.PageData = await dashboardRepository.GetPageDataByName("Survey");
            return View(await dashboardRepository.GetAsync());
        }

        [HttpPost]
        public async Task<ActionResult> PostRequest(string viewModel, string startTime)
        {
            dynamic showMessageString;
            try
            {
                //DateTime df = DateTime.MinValue;
                //if (!string.IsNullOrEmpty(startTime))
                //{
                //    df = DateTime.ParseExact(startTime, "dd/MM/yyyy hh:mm:ss tt", null, DateTimeStyles.None);
                //}
                DateTime df = DateTime.MinValue;
                if (!string.IsNullOrEmpty(startTime))
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB"); //dd/MM/yyyy hh:mm:ss
                    var convertedDate = DateTime.Parse(startTime);
                    df = convertedDate;
                }
                List<QuestionJsonViewModel> obj = new JavaScriptSerializer().Deserialize<List<QuestionJsonViewModel>>(viewModel);
                List<QuestionSubmitDto> submition = new List<QuestionSubmitDto>();
                var date = DateTime.UtcNow;

                string UserId = Guid.NewGuid().ToString();
                foreach (var item in obj)
                {
                    submition.Add(new QuestionSubmitDto
                    {
                        CategoryId = item.CategoryId,
                        CurrentUserId = UserId,
                        CreatedDate = date,
                        StartTime = df,
                        EndTime = date,
                        QuestionId = item.QuestionId,
                        QuestionOptionId = item.QuestionOptionId,
                        Answer = item.Text
                    });
                }
                var result = await formRepository.Save(submition);
                if (result == true) showMessageString = new { status = "success", message = "Thank You." };
                else showMessageString = new { status = "error", message = "error" };
            }
            catch (Exception ex) { showMessageString = new { status = "error", message = "error" }; }
            return Json(showMessageString);
        }

        public async Task<ActionResult> ThankYou() => View(await dashboardRepository.GetGraphData());

        public async Task<ActionResult> Sorry()
        {
            ViewBag.PageData = await dashboardRepository.GetPageDataByName("Sorry");
            return View();
        }
    }
}