﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using VaccineSurvey.ViewModel;

namespace VaccineSurvey.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController() { }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get { return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>(); }
            private set { _signInManager = value; }
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid) { return View(model); }
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return await RedirectToLocalAsync(returnUrl, model.Email);
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> LogOff()
        {
            var appUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
            if (appUser == null)
            {
                return RedirectToAction("Index", "Error");
            }
            appUser.UpdatedDate = DateTime.Now;
            await UserManager.UpdateAsync(appUser);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        #region Helpers 
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        private async Task<ActionResult> RedirectToLocalAsync(string returnUrl, string Email)
        {
            var user = await UserManager.FindByEmailAsync(Email);
            var Roles = await UserManager.GetRolesAsync(user.Id);
            if (Roles.Count() > 0)
            {
                if (Roles.FirstOrDefault().ToLower() == "admin")
                {
                    if (Url.IsLocalUrl(returnUrl)) { return Redirect(returnUrl); }
                    return RedirectToAction("Index", "Dashboard");
                }
            }
            return RedirectToAction("Index", "Categories");
        }

        #endregion
    }
}