using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using VaccineSurvey.Web.App_Start;

namespace VaccineSurvey.Web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Bootstrapper.Run();
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            HttpContextBase currentContext = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(currentContext);
            var LanguageCultureName = LanguageMangager.IsLanguageAvailable(routeData.Values["lang"].ToString());
            Thread.CurrentThread.CurrentCulture = new CultureInfo(LanguageCultureName);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(LanguageCultureName);
        }
    }
}
