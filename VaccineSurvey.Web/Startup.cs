﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VaccineSurvey.Web.Startup))]
namespace VaccineSurvey.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
