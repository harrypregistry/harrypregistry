var data_needs_saving = false;
jQuery(document).ready(function ($) { $('#preloader').fadeOut('1000', function () { $(this).remove(); }); });

$(function () {
    $("#LanguageS li").unbind().bind('click', function () {
        var t = window.location.pathname.replace(new RegExp('/' + $('#current li:first-child').attr('data-locale'), 'g'), "");
        if (t === '/') t = '';
        if (data_needs_saving) { ShowMessage("/" + $(this).attr('data-locale') + "/Home"); }
        else { window.location.href = "/" + $(this).attr('data-locale') + t; }
    });

    $("#widget").hover(function () { $(this).addClass('active') }, function () { $(this).removeClass('active') });

    var i = $(".classy-nav-container"),
        o = $(".classynav ul"),
        n = $(".classynav > ul > li"),
        s = $(".classy-navbar-toggler"),
        r = $(".classycloseIcon"),
        a = $(".navbarToggler"),
        l = $(".classy-menu"),
        c = $(window);

    s.on("click", function () { a.toggleClass("active"), l.toggleClass("menu-on") });
    r.on("click", function () { l.removeClass("menu-on"), a.removeClass("active") });
    n.has(".dropdown").addClass("cn-dropdown-item"), n.has(".megamenu").addClass("megamenu-item"), o.find("li a").each(function () { $(this).next().length > 0 && ($(this).parent("li").addClass("has-down"), $(this).parent("li").addClass("has-down")) });
    c.on("resize", function () { l.removeClass("menu-on"), a.removeClass("active") });
});
window.onbeforeunload = function () { if (data_needs_saving) { return $("#LeavePageMessage").val(); } else { return; } };