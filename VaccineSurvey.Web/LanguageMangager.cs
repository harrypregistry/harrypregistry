﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace VaccineSurvey.Web
{
    public class LanguageMangager
    {
        public static List<Languages> AvailableLanguages = new List<Languages> {
            new Languages { LanguageFullName = "English", LanguageCultureName = "en-US", LanguageCultureNameS = "en", LanguageDirection="ltr" },
            new Languages { LanguageFullName = "Português", LanguageCultureName = "pt-BR", LanguageCultureNameS = "pt", LanguageDirection="ltr" },
            //new Languages { LanguageFullName = "汉语", LanguageCultureName = "zh-CH", LanguageCultureNameS = "zh", LanguageDirection="ltr" },
            new Languages { LanguageFullName = "Italiano", LanguageCultureName = "it-IT", LanguageCultureNameS = "it", LanguageDirection="ltr" },
            new Languages { LanguageFullName = "Français", LanguageCultureName = "fr-FR", LanguageCultureNameS = "fr", LanguageDirection="ltr" },
            new Languages { LanguageFullName = "Español", LanguageCultureName = "es-ES", LanguageCultureNameS = "es", LanguageDirection="ltr" },
            //new Languages { LanguageFullName = "Deutsch", LanguageCultureName = "de-DE", LanguageCultureNameS = "de", LanguageDirection="ltr" },
            //new Languages { LanguageFullName = "العربية", LanguageCultureName = "ar-AE", LanguageCultureNameS = "ar", LanguageDirection="rtl" },
            new Languages { LanguageFullName = "русский", LanguageCultureName = "ru-RU", LanguageCultureNameS = "ru", LanguageDirection="ltr" },
            //new Languages { LanguageFullName = "한국어", LanguageCultureName = "ko-KR", LanguageCultureNameS = "ko", LanguageDirection="ltr" },
            //new Languages { LanguageFullName = "اردو", LanguageCultureName = "ur-PK", LanguageCultureNameS = "ur", LanguageDirection="rtl" },
            //new Languages { LanguageFullName = "Türkçe", LanguageCultureName = "tr-TR", LanguageCultureNameS = "tr", LanguageDirection="ltr" }
           // new Languages { LanguageFullName = "हिन्दी", LanguageCultureName = "hi-IN", LanguageCultureNameS = "hi", LanguageDirection="ltr" },
           // new Languages { LanguageFullName = "मराठी", LanguageCultureName = "mr-IN", LanguageCultureNameS = "mr", LanguageDirection="ltr" }
        };

        public static string IsLanguageAvailable(string lang)
        {
            var language = AvailableLanguages.Where(a => a.LanguageCultureName.Equals(lang) || a.LanguageCultureNameS.Equals(lang)).FirstOrDefault();
            if (language != null) { return language.LanguageCultureName; }
            else return AvailableLanguages[0].LanguageCultureName;
        }

        public static Languages GetLanguage(string lang)
        {
            var language = AvailableLanguages.Where(a => a.LanguageCultureName.Equals(lang) || a.LanguageCultureNameS.Equals(lang)).FirstOrDefault();
            if (language != null) { return language; }
            else return AvailableLanguages[0];
        }

        public string GetCurrentLanguage() => IsLanguageAvailable(CultureInfo.CurrentCulture.Name);

        public static string GetPropValue(object src, string propertyName)
        {
            try
            {
                string Language = IsLanguageAvailable(CultureInfo.CurrentCulture.Name).Split('-')[0];
                if (Language != "" && Language != "en") propertyName += "_" + Language.ToUpper();
                //return src.GetType().GetProperty(propertyName).GetValue(src, null).ToString().Replace("'", @"\'");

                var r = src.GetType().GetProperty(propertyName).GetValue(src, null);
                if (r != null) { return r.ToString().Replace("'", @"\'"); }
                return "";
            }
            catch { return ""; }
        }
        public static PropertyInfo GetSortExpression(object src, string propertyName)
        {
            string Language = IsLanguageAvailable(CultureInfo.CurrentCulture.Name).Split('-')[0];
            if (Language != "" && Language != "en") propertyName += "_" + Language.ToUpper();

            var pi = src.GetType().GetProperty(propertyName);
            return pi;
        }
    }

    public class Languages
    {
        public string LanguageFullName { get; set; }
        public string LanguageCultureName { get; set; }
        public string LanguageCultureNameS { get; set; }
        public string LanguageDirection { get; set; }
    }
}